import React from 'react';

const Base = React.lazy(() => import('../containers/Base'));
const Login = React.lazy(() => import('../containers/Login'));
const ChangePassword = React.lazy(() => import('../containers/ChangePassword'));
const ResetPassword = React.lazy(() => import('../containers/ResetPassword'));
const Dashboard = React.lazy(() => import('../containers/Dashboard'));
const Seller = React.lazy(() => import('../containers/Sellers'));
const SellerDetails = React.lazy(() => import('../containers/Sellers/sellerDetails'));
const SellerSettlements = React.lazy(() => import('../containers/Sellers/sellerSettlements'));
const SellerMenu = React.lazy(() => import('../containers/Sellers/menu'));
const Settings = React.lazy(() => import('../containers/Settings'));
const Orders = React.lazy(() => import('../containers/Orders'));
const OrderDetails = React.lazy(() => import('../containers/Orders/orderDetails'));
const Categories = React.lazy(() => import('../containers/Categories'));
const RawProducts = React.lazy(() => import('../containers/RawProducts'));
const Users = React.lazy(() => import('../containers/Users'));


const Admins = React.lazy(() => import('../containers/Admins'));
const Notification = React.lazy(()=> import('../containers/Notification'))
const Hotlines = React.lazy(()=> import('../containers/Hotlines/Hotlines'))
const HotlineDetails = React.lazy(()=> import('../containers/Hotlines/HotlineDetails'))
const Feedback = React.lazy(()=> import('../containers/Feedback'))
const AppVersion = React.lazy(()=> import('../containers/AppVersion'))

const routes = [
  { path: '/', exact: true, name: 'Base', component: Base, authRequired: false },
  { path: '/login', exact: true, name: 'Login', component: Login, authRequired: false },
  { path: '/change-password', exact: true, name: 'Change Password', component: ChangePassword, authRequired: true },
  { path: '/confirm-password-reset', exact: true, name: 'Reset Password', component: ResetPassword, authRequired: false },
  { path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard, authRequired: true },
  { path: '/sellers', exact: true, name: 'Sellers', component: Seller, authRequired: true },
  { path: '/sellers/details/:id', exact: true, name: 'Seller Details', component: SellerDetails, authRequired: true },
  { path: '/sellers/details/:id/settlements', exact: true, name: 'Seller Settlements', component: SellerSettlements, authRequired: true },
  { path: '/sellers/menu/:id', exact: true, name: 'Seller Menu', component: SellerMenu, authRequired: true },
  { path: '/settings', exact: true, name: 'Settings', component: Settings, authRequired: true },
  { path: '/orders', exact: true, name: 'Orders', component: Orders, authRequired: true },
  { path: '/orders/details/:id', exact: true, name: 'Order Details', component: OrderDetails, authRequired: true },
  { path: '/categories', exact: true, name: 'Categories', component: Categories, authRequired: true },
  { path: '/raw-products', exact: true, name: 'RawProducts', component: RawProducts, authRequired: true },
  { path: '/users', exact: true, name: 'Users', component: Users, authRequired: true },



  { path: '/admins', exact: true, name: 'Admins', component: Admins, authRequired: true },
  { path: '/hotlines', exact: true, name: 'Hotline Page', component: Hotlines , authRequired: true },
  { path: '/hotlines/details/:id', exact: true, name: 'Hotline Deatils Page', component: HotlineDetails , authRequired: true },
  { path: '/feedbacks', exact: true, name: 'Feedback Page', component: Feedback , authRequired: true },
  { path: '/notifications', exact: true, name: 'Notification Page', component: Notification, authRequired: true },
  { path: '/app-version', exact: true, name: 'App Version Page', component: AppVersion, authRequired: true },
];

export default routes;