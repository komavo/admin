import axios from 'axios';
import { store } from '../store';
import * as actionTypes from "../store/actions";

// const axiosInstance = axios.create({
//     baseURL: process.env.REACT_APP_ADMIN_API_URL,
//     timeout: 100000,
//     headers: {
//         'Content-Type': 'application/json',
//         'accept': 'application/json'
//     }
// });

export const apiCall = async (url = false, options = null) => {
  return new Promise(async (resolve, reject) => {
    try {
      if(!url) {
        return reject("URL required!");
      }
      const state = store.getState();
      if(!state.isAuthenticated || !state.user || !state.user.access_token) {
        store.dispatch({type: actionTypes.DESTROY_TOKENS});
        return reject("Your session expired, Please login again!");
      }

      const config = {
        baseURL: process.env.REACT_APP_ADMIN_API_URL,
        method: options.method ? options.method : 'get',
        url: `${url}`,
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
          'Accept': 'application/json',
          'Authorization': `Bearer ${state.user.access_token}`
        }
      };

      if(options.params) {
        config.params = {...options.params};
      }

      if(options.body) {
        config.data = {...options.body};
      }

      const response = await axios({...config});
      return resolve(response.data);
    } catch(err) {
      if(err.response.status === 403) {
        store.dispatch({type: actionTypes.DESTROY_TOKENS});
        return reject("Your session expired, Please login again!");
      } else {
        return reject("Something went wrong, Please try again!");
      }
    }
  });
}
