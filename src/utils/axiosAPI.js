import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_ADMIN_API_URL,
    timeout: 100000,
    headers: {
        'Content-Type': 'application/json',
        'accept': 'application/json'
    }
});

export default axiosInstance
