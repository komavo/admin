const SAMPLE = Object.freeze({
  sampleValue: "Some constant sample value"
});

export default {
  SAMPLE
};