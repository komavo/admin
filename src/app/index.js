import React, { Component, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Aux from '../hoc/_Aux';
import Loader from '../components/Loader';
import ScrollToTop from '../components/ScrollToTop';
import DefaultLayout from '../layout/default';
import DashboardLayout from '../layout/dashboard';
import '../assets/scss/custom.scss';

class App extends Component {
	render() {
		return (
			<Aux>
				<ScrollToTop>
					<Suspense fallback={<Loader />}>
						<Switch>
							<Route exact path='/dashboard' component={DashboardLayout} />
							<Route exact path='/sellers/menu/:id' component={DashboardLayout} />
							<Route exact path='/sellers/details/:id' component={DashboardLayout} />
							<Route exact path='/sellers/details/:id/settlements' component={DashboardLayout} />
							<Route exact path='/sellers' component={DashboardLayout} />
							<Route exact path='/settings' component={DashboardLayout} />
							<Route exact path='/orders/details/:id' component={DashboardLayout} />
							<Route exact path='/orders' component={DashboardLayout} />
							<Route exact path='/categories' component={DashboardLayout} />
							<Route exact path='/raw-products' component={DashboardLayout} />
							<Route exact path='/users' component={DashboardLayout} />

							<Route exact path='/change-password' component={DashboardLayout} />
							<Route exact path='/login' component={DefaultLayout} />
							<Route exact path='/admins' component={DashboardLayout} />
							<Route exact path='/notifications' component={DashboardLayout} />
							<Route exact path='/hotlines' component={DashboardLayout} />
							<Route exact path='/hotlines/details/:id' component={DashboardLayout} />
							<Route exact path='/feedbacks' component={DashboardLayout} />
							<Route exact path='/app-version' component={DashboardLayout} />
							<Route exact path='/confirm-password-reset' component={DefaultLayout} />
							<Route exact path='/' component={DefaultLayout} />
							<Redirect to='/' />
						</Switch>
					</Suspense>
				</ScrollToTop>
			</Aux>
		);
	}
}

export default App;