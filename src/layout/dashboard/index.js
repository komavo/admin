import React, { Component, Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import routes from '../../routes';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import Sidebar from '../../components/Sidebar';
import Header from '../../components/Header';
import './index.scss';

class DefaultLayout extends Component {
  render() {
    const pages = routes.map((route, index) => {
      return (route.component) ? (
        <Route
          key={index}
          path={`${route.path}`}
          exact={route.exact}
          name={route.name}
          render={props => (
            <route.component {...props} />
          )} />
      ) : (null);
    });
    return this.props.isAuthenticated ? (
      <Aux>
          <div className="wrapper">
            <Sidebar />
            <div id='content' style={{ overflow : 'auto'}}>
              <Header/>
              <Suspense fallback={<Loader />}>
                <Switch>
                    {pages}
                </Switch>
              </Suspense>
            </div>
          </div>
      </Aux>
    )
    : <Redirect to="/login" />
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    isAuthenticated: state.isAuthenticated,
  }
};

const mapDispatchToProps = dispatch => {
  return { }
};

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);