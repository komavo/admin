import React, { Component, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import routes from '../../routes';
import './app.scss';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';

class DefaultLayout extends Component {
  render() {
    const pages = routes.map((route, index) => {
      return (route.component) ? (
        <Route
          key={index}
          path={`${route.path}`}
          exact={route.exact}
          name={route.name}
          render={props => (
            <route.component {...props} />
          )} />
      ) : (null);
    });
    return (
      <Aux>
            <Suspense fallback={<Loader />}>
              <Switch>
                {pages}
              </Switch>
            </Suspense>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    isAuthenticated: state.isAuthenticated,
  }
};

const mapDispatchToProps = dispatch => {
  return { }
};

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);