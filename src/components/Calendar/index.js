import React, { useState, useEffect } from "react";
import { Modal, Button } from 'react-bootstrap';
import { DateRange } from 'react-date-range';

import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';

import "./index.scss";

const Calender = (props) => {
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: 'selection'
    }
  ]);

  return (
    <Modal className="calender-modal" show={props.show ? true : false} onHide={props.hide}>
      <Modal.Body>
        <DateRange
          editableDateInputs={true}
          onChange={item => setState([item.selection])}
          moveRangeOnFirstSelection={false}
          ranges={state}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" onClick={props.hide}>
          Cancel
        </Button>
        <Button variant="primary" onClick={() => props.onSelect(state[0])}>
          Apply
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default Calender;