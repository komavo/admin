import React, { useEffect, useState } from "react";
import moment from 'moment';
import _ from 'lodash';
import { Table, InputGroup, FormControl } from 'react-bootstrap';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import { BiSortDown, BiSortUp, BiSearch, BiXCircle } from 'react-icons/bi';
import ReactPaginate from 'react-paginate';

import { apiCall } from "../../utils/services";

import Loader from "../Loader";
import Calendar from "../Calendar";

import "./index.scss";

const DataTable = (props) => {
  const [loading, setLoading] = useState(false);
  const [showCalendar, setShowCalendar] = useState(false);
  const [data, setData] = useState({});
  const [params, setParams] = useState({
    page: 1,
    limit: 10
  });

  const getData = async () => {
    setLoading(true);
    const response = await apiCall(props.baseUrl, {
      method: 'get',
      params: { ...params }
    });
    setData(response);
    setLoading(false);

    if (props.setTotal) {
      props.setTotal(response.count);
    }
  }

  useEffect(() => {
    getData();
  }, [params]);

  useEffect(() => {
    if(Number(props.refresh) > 0) {
      getData();
    }
  }, [props.refresh]);

  const formatColumn = (item, column) => {
    switch (column.type) {
      case 'datetime':
        return <span className="text-nowrap">{moment(_.get(item, column.key)).format('lll')}</span>
      case 'date':
          return <span className="text-nowrap">{moment(_.get(item, column.key)).format('MMM DD, YYYY')}</span>
      default:
        return <span className={`d-block text-truncate`}>{_.get(item, column.key)}</span>
    }
  }

  const getRow = (item, index) => {
    return (
      <tr key={`table-row-${index}`} onClick={() => props.onRowClick ? props.onRowClick(item) : null}>
        <>
          {props.columns.map((column, index) => {
            return (
              <td key={`table-col-${index}`} className={`${column.align ? column.align : 'text-left'}`} style={{ ...column.style }}>
                {formatColumn(item, column)}
              </td>
            )
          })}
          {props.actions && props.actions.length > 0 &&
            <td className="text-right">
              {props.actions.map((action, i) => {
                return (
                  <button
                    key={`action-btn-${i}`}
                    className={`btn btn-sm ml-2 ${action.class ? action.class : 'btn-info'}`}
                    onClick={(e) => { e.preventDefault(); e.stopPropagation(); action.onClick(item) }}
                    title={action.tooltip ? action.tooltip : ""}
                  >
                    {action.icon}
                  </button>
                )
              })}
            </td>
          }
        </>
      </tr>
    )
  }

  const renderRows = () => {
    return data.results.map((item, index) => getRow(item, index));
  }

  const handlePageClick = (page) => {
    setParams({
      ...params,
      page: page.selected + 1
    })
  }

  const handleSort = (sortBy) => {
    if (!loading) {
      const temp = { ...params };
      if (!temp.sortBy || temp.sortBy !== sortBy) {
        temp.page = 1;
        temp.sortBy = sortBy;
        temp.order = 'desc';
      } else if (temp.sortBy && temp.sortBy === sortBy) {
        temp.page = 1;
        temp.order = temp.order === 'desc' ? 'asc' : 'desc';
      }

      setParams({ ...temp });
    }
  }

  const handleSearch = (e) => {
    e.preventDefault();
    const value = e.target.value;
    const temp = { ...params };
    temp.page = 1;
    delete temp['search'];
    if (value.length > 0) {
      temp['search'] = value;
    }
    setParams({ ...temp });
  }

  const handleClearSearch = () => {
    const temp = { ...params };
    temp.page = 1;
    delete temp['search'];
    setParams({ ...temp });
  }

  const handleOnFilter = (e) => {
    e.preventDefault();
    const temp = { ...params };
    const name = e.target.name;
    const value = e.target.value;
    temp.page = 1;
    delete temp[name];

    if (value.length > 0) {
      temp[name] = value;
    }
    setParams({ ...temp });
  }

  const handleOnDateChange = (name, range = null) => {
    const temp = { ...params };
    temp.page = 1;
    delete temp[name];
    if (range) {
      temp[name] = `${moment(range['startDate']).format('MM/DD/YYYY')}-${moment(range['endDate']).format('MM/DD/YYYY')}`;
    }
    setParams({ ...temp });
    setShowCalendar(false);
  }

  const getFilterInput = (field) => {
    switch (field.type) {
      case 'select':
        return (
          <FormControl name={field.name} as="select" size="sm" onChange={handleOnFilter}>
            <option value="">{field.placeholder ? field.placeholder : ""}</option>
            {field.options && field.options.length > 0 && field.options.map(x => <option key={`option-${x.value}`} value={x.value}>{x.text}</option>)}
          </FormControl>
        )
      case 'daterange':
        return (
          <span className="d-block position-relative">
            <FormControl
              className="fs-12"
              onFocus={(e) => { setShowCalendar(field.name); e.target.blur(); }}
              value={params[field.name] ? params[field.name] : ""}
              readOnly={true}
              placeholder={field.placeholder ? field.placeholder : ""}
            />
            {params[field.name] &&
              <BiXCircle size="14" className="position-absolute cursor-pointer text-danger" style={{ right: 7, top: 9 }} onClick={() => handleOnDateChange(field.name, null)} />
            }
          </span>
        )
      default:
        return null;
    }
  }

  return (
    <>
      <div className="d-flex align-items-center justify-content-start pb-2 table-toolbar">
        {props.searchable &&
          <span className="mr-3">
            <InputGroup className={`mb-0 search-input--wrapper ${params.search ? 'hasValue' : ''}`}>
              <InputGroup.Prepend>
                <InputGroup.Text id="search-input" className="text-primary"><BiSearch size="12" /></InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="Search"
                aria-label="Search"
                aria-describedby="search-input"
                className="fs-12"
                onChange={handleSearch}
                value={params.search ? params.search : ""}
              />
              {params.search && <InputGroup.Append >
                <InputGroup.Text className="text-danger" onClick={handleClearSearch}><BiXCircle size="14" /></InputGroup.Text>
              </InputGroup.Append>}
            </InputGroup>
          </span>
        }
        {props.filters && props.filters.length > 0 &&
          <span className="d-flex align-items-center justify-content-start">
            {props.filters.map((x, i) => <span key={`filters-${i}`} className="mr-3">{getFilterInput(x)}</span>)}
          </span>
        }
      </div>
      <Table size={props.size ? props.size : 'sm'} hover={props.onRowClick ? true : false}>
        <thead>
          <tr>
            {props.columns.map((column, index) => {
              return (
                <th
                  onClick={() => column.sortable && column.sortable ? handleSort(column.key) : null}
                  key={`table-head-${index}`}
                  className={`${column.align ? column.align : 'text-left'} ${column.sortable && column.sortable ? 'cursor-pointer' : ''}`}
                >
                  <span className={`position-relative`}>
                    {column.text}
                    {column.sortable && column.sortable &&
                      <>
                        {params.sortBy && params.sortBy === column.key && params.order === 'asc' ? <BiSortUp size="20" className="ml-2" /> : <BiSortDown size="20" className="ml-2" />}
                      </>
                    }
                  </span>
                </th>
              )
            })}
            {props.actions && props.actions.length > 0 && <th className="text-right">Action</th>}
          </tr>
        </thead>
        <tbody>
          {loading && <tr className="table-row-loading"><td colSpan={(props.columns.length + 1)} className="text-center py-5"><Loader size="sm" animation="fast" /></td></tr>}
          {!loading && data.results && data.results.length === 0 && <tr><td colSpan={(props.columns.length + 1)} className="text-center py-5">No records founds!</td></tr>}
          {!loading && data.results && data.results.length > 0 && renderRows()}
        </tbody>
      </Table>
      {data.results && data.results.length > 0 && <div>
        <ReactPaginate
          previousLabel={<FiChevronLeft size={20} />}
          nextLabel={<FiChevronRight size={20} />}
          initialPage={0}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={Math.ceil(data.count / params.limit)}
          marginPagesDisplayed={2}
          pageRangeDisplayed={0}
          onPageChange={(page) => handlePageClick(page)}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
      </div>}
      <Calendar
        show={showCalendar}
        hide={() => setShowCalendar(false)}
        onSelect={(range) => handleOnDateChange(showCalendar, range)}
      />
    </>
  )
}

export default DataTable;