import React, { useEffect, useState } from 'react';
import { Modal, Table, Button, Form } from 'react-bootstrap';
import Loader from '../Loader';
import './index.scss';
import { FiX, FiPlus } from 'react-icons/fi';
import 'rc-time-picker/assets/index.css';
import axiosInstance from '../../utils/axiosAPI';
import moment from "moment";

export default function CommentReplyModal(props) {
    const handleFormSubmit = (e) =>{
        e.preventDefault();
        let data = { 
            reply_content: e.target.comment_reply.value
        }
        let url;
        if(props.commentID){
            url = '/posts/' + props.commentID + '/admin-reply/'
        }
        else{
            url = '/posts/' + props.postID + '/admin-comment/'
        }
        axiosInstance.post(url, data, { headers: { 'Authorization': 'Bearer ' + props.access_token } })
        .then(res => {
            if(props.commentID){
                props.setReply(res.data);
            }
            else{
                props.setComment(res.data);
            }
            props.hide();
        })

    }
    return !props.show ? null : (
        <Modal className='comment-reply-modal' show={props.show} onHide={props.hide}>
            <Modal.Body>
                Add reply
                <Form onSubmit={(e) => handleFormSubmit(e)} className='mt-2'>
                    <Form.Group>
                        <Form.Label>Comment</Form.Label>
                        <Form.Control as="textarea" name='comment_reply' className='mw-100'/>
                    </Form.Group>
                    <Button variant='primary' size='sm' type='submit'>Submit</Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
}