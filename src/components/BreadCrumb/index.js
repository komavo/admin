import React from 'react';
import { Link } from 'react-router-dom';

export default function BreadCrumb(props) {
    return !props.list ? null : (
                <div className='breadcrumbs'>
                    { props.list.map((item, index) => {
                        return <Link key={index} to={item.path} className={ item.status && item.status === "active" ? "breadcrumb-item active" : "breadcrumb-item"}>{item.location}</Link>
                    })}
                </div>
    );
}