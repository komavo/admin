import React, { useEffect } from 'react';
import Loader from '../Loader';
import { Card, Row, Col, Button, Modal, Form, Spinner } from 'react-bootstrap';
import { FiTrash, FiEdit2 } from 'react-icons/fi';
import TimePicker from 'rc-time-picker';
import './index.scss';
import moment from "moment";
import axiosInstance from '../../utils/axiosAPI';


export default function ContactDisplay(props) {
    const [cleanedData, setCleanedData] = React.useState();
    const [loading, setLoading] = React.useState(true);
    const [categoryID, setCategoryID] = React.useState();
    const [editTimeModal, setEditTimeModal] = React.useState();
    const [editTimeData, setEditTimeData] = React.useState();
    const [editTimeBtnLoading, setEditTimeBtnLoading] = React.useState(false);

    useEffect(() => {
        const cleaned = []
        for (let i of props.category.contacts) {
            let temp = {}
            if (cleaned.some(el => el.contact_number === i.contact_number)) {
                let index = cleaned.indexOf(cleaned.find(el => el.contact_number === i.contact_number))
                temp = {
                    contact_id: i.contact_id,
                    active_from_day: i.active_from_day,
                    active_from_time: i.active_from_time,
                    active_to_day: i.active_to_day,
                    active_to_time: i.active_to_time,
                }
                cleaned[index].timings.push(temp);
            }
            else {
                temp.contact_number = i.contact_number;
                temp.contact_name = i.contact_name;
                temp.timings = [{
                    contact_id: i.contact_id,
                    active_from_day: i.active_from_day,
                    active_from_time: i.active_from_time,
                    active_to_day: i.active_to_day,
                    active_to_time: i.active_to_time,
                }]
                cleaned.push(temp);
            }
        }
        setLoading(false);
        setCategoryID(props.category.category_id)
        setCleanedData(cleaned);
    }, [props.category]);
    const handleDeleteContact = (contact_number) => {
        const temp = {
            contact_number: contact_number,
            category_id: categoryID
        }
        props.deleteContact(temp);
    }
    const handleDeleteTiming = (contact_id) => {
        props.deleteTiming(contact_id);
    }
    const handleEditContact = (contact_number) => {
        const temp = {
            contact_number: contact_number,
            category_id: categoryID
        }
        props.editContact(temp);
    }
    const showEditTime = (contact_id) => {
        setEditTimeData(props.category.contacts.find(el => el.contact_id === contact_id));
        console.log(props.category.contacts.find(el => el.contact_id === contact_id));
        setEditTimeModal(true);
    }
    const closeEditTime = () => {
        setEditTimeModal(false);
        setEditTimeData(null);
    }
    const handleEditTime = (e) =>{
        e.preventDefault();
        setEditTimeBtnLoading(true);
        let data = {
            active_from_time : e.target.active_from_time.value, 
            active_to_time : e.target.active_to_time.value
        }
        axiosInstance.patch('/hotlines/' + editTimeData.contact_id + '/update-timing/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
            .then(res => {
                window.location.reload();
            })
    }
    return (
        <>
            { cleanedData && cleanedData.map((item, index) => {
                return (
                    <Card key={index} className='py-3 px-4 mt-3 mx-2'>
                        <span className='h3'>{item.contact_number}</span>
                        <span className='text-secondary'>{item.contact_name}</span>
                        <Row className='mt-3'>
                            {item.timings.length > 0 && item.timings.map((timing, i) => {
                                return (
                                    <Col sm={12} md={3} className='mt-3' key={`timing-${timing.contact_id}`}>
                                        <div className='p-2 hotline-card'>
                                            <div className='d-flex flex-column align-items-center justify-content-center'>
                                                <div className='d-block w-100 p-2' style={{ backgroundColor: "#B4CFFF", borderRadius: "7px" }}>
                                                    <div className='d-flex align-items-center justify-content-center hotline-card-day' style={{ textTransform: "uppercase", fontWeight: "600" }}>
                                                        {props.days[timing.active_from_day]}
                                                    </div>
                                                </div>
                                                <div className='fs-25 mt-3'>{timing.active_from_time.substring(0, timing.active_to_time.length - 3)}</div>
                                                <span className='fs-13 text-secondary'>TO</span>
                                                <div className='fs-25'>{timing.active_to_time.substring(0, timing.active_to_time.length - 3)}</div>
                                                <Button variant="light" size='sm' className='mt-3' onClick={() => showEditTime(timing.contact_id)}><FiEdit2 /> EDIT</Button>
                                            </div>
                                        </div>
                                    </Col>
                                )
                            })}
                        </Row>

                        <div className='d-flex justify-content-end align-items-center'>
                            {/* <Button className='mt-5 btn btn-light btn-sm mr-2' onClick={() => handleEditContact(item.contact_number)}><FiEdit2 /> Edit</Button> */}
                            <Button variant='danger' size='sm' className='mt-5' onClick={() => handleDeleteContact(item.contact_number)} ><FiTrash strokeWidth='4' className='mr-2' /> Delete this contact</Button>
                        </div>
                    </Card>
                )
            })
            }

            {editTimeData && <Modal show={editTimeModal} onHide={closeEditTime} className='edit-time-modal'>
                <Modal.Body>
                    <div className='fs-20'>Update time</div>
                    <div className='mt-3' style={{ fontSize: "2rem"}}>{props.days[editTimeData.active_from_day]}</div>
                    <Form onSubmit={(e) => handleEditTime(e)}>
                        <Row>
                            <Col>
                                <div className='d-flex justify-content-center align-items-center flex-column mt-3 mb-3'>
                                    <TimePicker
                                        popupClassName='timepicker'
                                        placement="topRight"
                                        placeholder="Opens"
                                        name='active_from_time'
                                        defaultValue={moment(editTimeData.active_from_time, "HH:mm:ss")}
                                    />
                                    <span className='fs-12 mt-2 text-secondary'>OPENS</span>
                                </div>
                            </Col>
                            <Col>
                                <div className='d-flex justify-content-center align-items-center flex-column mt-3 mb-3'>
                                    <TimePicker
                                        popupClassName='timepicker'
                                        placement="topRight"
                                        placeholder="Closes"
                                        name='active_to_time'
                                        defaultValue={moment(editTimeData.active_to_time, "HH:mm:ss")}
                                    />
                                    <span className='fs-12 mt-2 text-secondary'>CLOSES</span>
                                </div>
                            </Col>
                        </Row>
                        <div className='d-flex align-items-center justify-content-end mt-3'>
                            <Button disabled={editTimeBtnLoading} variant="primary" size='sm' type='submit'>
                                { editTimeBtnLoading 
                                    ? <><Spinner size='sm' className='mr-2' animation='border'/> Please wait</>
                                    : <>Save Changes</>
                                }
                            </Button>
                            <Button variant="danger" onClick={closeEditTime} size='sm' className='ml-2'>Cancel</Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
            }
        </>
    );
}