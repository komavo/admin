import React from 'react';

import './index.css';

export default function Loader(props) {
  return (
    <div className={`loader-container d-flex align-items-center justify-content-center ${props.absolute ? 'absolute' : ""} ${props.size ? `loader-variant-${props.size}` : "loader-variant-normal"}  ${props.animation ? `loader-animation-${props.animation}` : "loader-animation-normal"}`}>
      <div className='loader'>
        <div className="nb-spinner"></div>
      </div>
    </div>
  );
}