import React, { useEffect } from 'react';
import { Form, Button, Card, Row, Col, Spinner} from "react-bootstrap";
import { FiPlus, FiX } from "react-icons/fi";
import axiosInstance from '../../utils/axiosAPI';

const hotlineSchema = {
    contact_no: {
        type: "number",
        label: "Hotline Number*",
        value: ""
    },
    contact_name: {
        type: "text",
        label: "Hotline Name(optional)",
        value: ""
    },
    contact_description: {
        type: "textarea",
        label: "Hotline Description(optional)",
        value: ""
    },
    contact_24x7:{
        type: "checkbox",
        label: "",
        value: false
    },
    active_from_day: {
        type: "select",
        label: "Active From Day*",
        value: "",
        options: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    },
    active_from_time: {
        type: "time",
        label: "Active From Time*",
        value: ""
    },
    active_to_day: {
        type: "select",
        label: "Active To Day*",
        value: "",
        options: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    },
    active_to_time: {
        type: "time",
        label: "Active To Time*",
        value: ""
    },
};

export default function ContactsForm(props) {
    const [hotlines, setHotlines] = React.useState([{ ...hotlineSchema }]);
    const [hotlines247, setHotlines247] = React.useState([]);
    const [saveLoader, setSaveLoader] = React.useState(false);
    const [formValidated, setFormValidated] = React.useState(true);

    // useEffect(() => {
    //     if (props.contacts.length > 0) {
    //         console.log(props.contacts)
    //     }
    // }, [props.contacts])
    const handleAddMore = () => {
        const temp = [...hotlines];
        temp.push({ ...hotlineSchema });
        setHotlines([...temp]);
    }

    const getFieldInput = (field, index, key) => {
        switch (field.type) {
            case "select":
                return <Form.Control as="select" onChange={(event) => handleOnChange(event, index, key)} required>
                    {field.options.map((x, index) => <option key={index} value={x}>{x}</option>)}
                </Form.Control>
            case "time":
                return <input type="time" onChange={(event) => handleOnChange(event, index, key)} required/>
            case "checkbox":
                return <Form.Check type="checkbox" label="This is a 24x7 hotline" onChange={(event) => handleOnChange(event, index, key)}/>
            case "number":
                return <input type="number" onChange={(event) => handleOnChange(event, index, key)} required/>
            default:
                return <input type="text" onChange={(event) => handleOnChange(event, index, key)} required/>
        }
    }

    const handleOnChange = (event, index, key) => {
        let temp = [...hotlines]
        
        if(key === "contact_24x7"){
            temp[index][key].value = event.target.checked;
            temp[index]["active_from_day"].value = (event.target.checked ? "Monday" : "");
            temp[index]["active_from_time"].value = (event.target.checked ? "00:00" : "");
            temp[index]["active_to_day"].value = (event.target.checked ? "Sunday" : "");
            temp[index]["active_to_time"].value = (event.target.checked ? "59:59" : "");

            temp[index]["active_from_day"].isVisible = !event.target.checked;
            temp[index]["active_from_time"].isVisible = !event.target.checked;
            temp[index]["active_to_day"].isVisible = !event.target.checked;
            temp[index]["active_to_time"].isVisible = !event.target.checked;

            if(event.target.checked === true) {
                temp = [{...temp[index]}];
            }
      }
        else{
            temp[index][key].value = event.target.value;
        }
        setHotlines([...temp]);
    }

    const onSubmit = (e) => {
        e.preventDefault();
        setSaveLoader(true);
        const day_numbers = {
            "Monday" : 1,
            "Tuesday" : 2,
            "Wednesday" : 3,
            "Thursday" : 4,
            "Friday" : 5,
            "Saturday" : 6,
            "Sunday" : 7
        }
        let cleaned_contacts = [];
        console.log(hotlines)
        for(let contact of hotlines){
            let active_from_time = contact['active_from_time'].value; 
            let active_to_time = contact['active_to_time'].value; 
            if(active_from_time.length === 5){
                active_from_time += ':00'
            }
            if(active_to_time.length === 5){
                active_to_time += ':00'
            }

            const temp = {
                contact_name : contact['contact_name'].value,
                contact_number : contact['contact_no'].value,
                contact_description : contact['contact_description'].value,
                active_from_day : day_numbers[contact['active_from_day'].value],
                active_from_time : active_from_time,
                active_to_day : day_numbers[contact['active_to_day'].value],
                active_to_time : active_to_time,
            }
            cleaned_contacts.push(temp);
        }
        let data = {
            category_id : props.category_id,
            contacts : cleaned_contacts
        }
        setSaveLoader(false);
        // axiosInstance.post('/hotlines/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        // .then(res => {
        //   console.log(res.data);
        //   window.location.reload();
        // })
        // .catch(err => {
        //     console.log(err);
        // })
    }
    const handleDeleteContact = (index) =>{
        setHotlines([...hotlines.filter((item, i) => i === index)]);
    }

    const hasContact24x7 = () => {
        let exists = false;
        hotlines.forEach((x) => {
            Object.keys(x).forEach(y => {
                if(y === 'contact_24x7' && x[y].value === true) {
                    exists = true;
                    return false;
                }
            })
            if(exists) return false;
        });
        console.log(exists);
        return exists;
    }

    return (
        <div>
            {hotlines.map((item, index) => {
                return <Card key={index} className="px-4 mt-3 mx-2">
                    <Form className="mt-3" onSubmit={(event) => event.preventDefault()}>
                        {Object.keys(item).map((fieldKey, fieldIndex) => {
                            return item[fieldKey].isVisible === undefined || item[fieldKey].isVisible !== false ? (
                                <Form.Group key={`${fieldKey}-${fieldIndex}`} as={Row}>
                                        <Form.Label column sm="2">{item[fieldKey].label}:</Form.Label>
                                        <Col sm="10">
                                                {getFieldInput(item[fieldKey], index, fieldKey)}
                                        </Col>
                                </Form.Group>
                            ) : null;
                        })}
                        {!hasContact24x7() &&
                            <>
                                {(index + 1) === hotlines.length ?
                                    <Button className="btn btn-success mt-3 mb-5 ml-1" onClick={() => handleAddMore()}><FiPlus /> Add Another Hotline</Button>
                                    :
                                    <Button className="btn btn-danger mt-3 mb-5 ml-1" onClick={() => handleDeleteContact(index)}><FiX /> Delete Hotline</Button>
                                }
                            </>
                        }
                    </Form>
                </Card>
            })}
            {!formValidated && <span className='p-3 text-danger fs-15'>* marked fields are required</span>}
            <br/>
            <Button className="btn btn-success mt-3 mb-5 ml-2" onClick={(event) => onSubmit(event)}>
                { saveLoader ?
                    <><Spinner variant="light" size="sm" className="mr-2" /> saving... </>
                :   <><FiPlus />Save</>
                }
            </Button>
        </div>
    );
}