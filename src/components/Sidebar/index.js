import React, { useState } from "react";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import "./sidebar.scss";
import "./../../assets/scss/main.scss";
import FullLogo from "./../../assets/images/full-logo.svg";
import Logo from "./../../assets/images/icon.png";
import {
  FiGrid,
  FiUsers,
  FiHexagon,
  FiPackage,
  FiServer
} from "react-icons/fi";
import { GiChickenLeg } from "react-icons/gi";

const Sidebar = (props) => {
  const location = props.history.location.pathname;
  const [sidebarExpanded, expandSidebar] = useState(false);
  const toggleSidebar = () => {
    expandSidebar(!sidebarExpanded);
  };

  return (
    <nav id="sidebar" className={!sidebarExpanded ? null : "active"}>
      <div
        className="p-3 d-flex align-items-center justify-content-start"
        style={{ cursor: "pointer" }}
        onClick={toggleSidebar}
      >
        {!sidebarExpanded ? (
          <span className="text-light">
            <img src={FullLogo} style={{ width: "150px" }} alt="LogoImage" />&nbsp;&nbsp;
            <i className="fa fa-chevron-left text-muted fa-sm"></i>
          </span>
        ) : (
          <>
          <img src={Logo} style={{ width: "25px" }} alt="LogoImage" />
            &nbsp;<i className="fa fa-chevron-right text-muted fa-sm"></i>
          </>
        )}
      </div>
      <div className='mt-3'></div>
      <Link to="/dashboard">
        <div
          className={
            location === "/dashboard"
              ? "px-3 py-2 sidemenu active"
              : "px-3 py-2 sidemenu"
          }
        >
          <FiGrid color="white" size="25px" />
          {!sidebarExpanded ? (<div style={{ marginLeft: 15}}>Dashboard</div>) : null}
        </div>
      </Link>


      <Link to="/sellers">
        <div
          className={
            location === "/sellers" ||
            location === `/sellers/details/${props.match.params.id}` ||
            location === `/sellers/menu/${props.match.params.id}` ||
            location === `/sellers/details/${props.match.params.id}/settlements`
              ? "px-3 py-2 sidemenu active"
              : "px-3 py-2 sidemenu"
          }
        >
          <FiHexagon color="white" size="25px" />
          {!sidebarExpanded ? (<div style={{ marginLeft: 15}}>Sellers</div>) : null}
        </div>
      </Link>
      <Link to="/orders">
        <div
          className={
            location === "/orders" || 
            location === "/orders/details/" + props.match.params.id 
              ? "px-3 py-2 sidemenu active"
              : "px-3 py-2 sidemenu"
          }
        >
          <FiPackage color="white" size="25px" />
          {!sidebarExpanded ? (<div style={{ marginLeft: 15}}>Orders</div>) : null}
        </div>
      </Link>

      <Link to="/users">
        <div
          className={
            location === "/users" || 
            location === "/users/details/" + props.match.params.id 
              ? "px-3 py-2 sidemenu active"
              : "px-3 py-2 sidemenu"
          }
        >
          <FiUsers color="white" size="25px" />
          {!sidebarExpanded ? (<div style={{ marginLeft: 15}}>Users</div>) : null}
        </div>
      </Link>

      <Link to="/categories">
        <div
          className={
            location === "/categories"
              ? "px-3 py-2 sidemenu active"
              : "px-3 py-2 sidemenu"
          }
        >
          <FiServer color="white" size="25px" />
          {!sidebarExpanded ? (<div style={{ marginLeft: 15}}>Categories</div>) : null}
        </div>
      </Link>
      <Link to="/raw-products">
        <div
          className={
            location === "/raw-products"
              ? "px-3 py-2 sidemenu active"
              : "px-3 py-2 sidemenu"
          }
        >
          <GiChickenLeg color="white" size="25px"/> 
          {!sidebarExpanded ? (<div style={{ marginLeft: 15}}>Raw Products</div>) : null}
        </div>
      </Link>
    </nav>
  );
};

const mapStateToProps = (state) => {
  return {
    defaultPath: state.defaultPath,
    user: state.user,
    isAuthenticated: state.isAuthenticated,
    toast: state.toast,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Sidebar)
);
