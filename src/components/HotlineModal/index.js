import React, { useEffect } from 'react';
import { Modal, Button, Form, Row, Col, Spinner, NavItem } from 'react-bootstrap';
import Loader from '../Loader';
import './index.scss';
import { FiX, FiPlus } from 'react-icons/fi';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import moment from "moment";

const panelSchema = () => {
    return {
        contact_number: null,
        contact_ids: [],
        days: [],
        time_slots: [],
        active_from_time: null,
        active_to_time: null
    }
}
const timeSchema = () => {
    return {
        active_from_time: null,
        active_to_time: null
    }
}

export default function HotlineModal(props) {

    const [saveLoader, setSaveLoader] = React.useState();
    const [hotlines, setHotlines] = React.useState([]);
    const [hotlineEmptyError, setHotlineEmptyError] = React.useState(false);
    const [loading, setLoading] = React.useState();

    // const [editData, setEditData] = React.useState([]);


    const days = {
        1: "Mon",
        2: "Tue",
        3: "Wed",
        4: "Thu",
        5: "Fri",
        6: "Sat",
        7: "Sun"
    }
    const updateHotlines = (editData) => {
        setLoading(true);
        console.log('creating schema->', editData);
        let temp = []
        // let editHotlines = editData.category.contacts.filter(item => item.contact_number === editData.contact_number);
        editData.category.contacts.forEach(function (item, index) {
            if (index === 0) {
                let schema = { ...panelSchema() };
                schema.contact_ids.push(item.contact_id);
                schema.contact_number = editData.contact_number;
                schema.days.push(item.active_from_day.toString());
                schema.time_slots.push({
                    active_from_time : item.active_from_time,
                    active_to_time : item.active_to_time
                })
                // schema.active_from_time = item.active_from_time;
                // schema.active_to_time = item.active_to_time;
                temp.push({ ...schema });
            }
            console.log(temp);
        });
        setHotlines(temp);
        setLoading(false);
    }
    
    useEffect(() => {
        setLoading(true);
        if (props.editData) {
            updateHotlines(props.editData);
        }
        else {
            let temp = {...panelSchema()};
            temp.time_slots = [{ ...timeSchema() }];
            setHotlines([{ ...temp}]);
        }
        setLoading(false);
    }, [props.editData])
    const passDataToParent = (e) => {
        e.preventDefault();
        if (hotlines.length === 0) {
            setHotlineEmptyError(true);
            return
        }
        setSaveLoader(true);

        let data = [];
        const contact_name = e.target.contact_name.value;
        const contact_number = e.target.contact_number.value;

        for (let item of hotlines) {
            console.log(item)
            item.days.forEach(function(day, index){
                item.time_slots.forEach(function(time, time_index){
                    data.push({
                        contact_id: item.contact_ids[index],
                        contact_number: contact_number,
                        contact_name: contact_name,
                        active_from_day: day,
                        active_to_day: day,
                        active_from_time: time.active_from_time,
                        active_to_time: time.active_to_time
                    })
                })
            })
        }

        setSaveLoader(false);
        // console.log(data);

        props.passData(data);
        props.hide();
        setHotlines([]);
    }
    const addTiming = () => {
        const temp_hotlines = [...hotlines];
        let new_schema = {...panelSchema()};
        new_schema.time_slots.push({...timeSchema()});
        temp_hotlines.push({...new_schema});
        setHotlines(temp_hotlines);
    }
    const addTimeSlot = (index) =>{
        let temp = hotlines;
        temp[index].time_slots.push({...timeSchema()});
        setHotlines([...temp]);
    }
    const removeTimeSlot = (index, time_index) =>{
        let temp = hotlines;
        temp[index].time_slots = temp[index].time_slots.filter((item, i) => i !== time_index);
        console.log(temp);
        setHotlines([...temp])
    }
    const handleInputChange = (e, type, index, time_index) => {
        const temp = [...hotlines];
        if (type === "checkbox") {
            const { value, checked } = e.target;
            if (checked) {
                temp[index].days.push(value);
                if (temp[index].days.length !== temp[index].contact_ids.length){
                    temp[index].contact_ids.push(0);
                }
            }
            else {
                temp[index].days = temp[index].days.filter((item, i) => {
                    if(item === value){
                        temp[index].contact_ids.splice(i, 1);
                    }
                    return item !== value;
                });
            }
        }
        else {
            const value = moment(e).format("HH:mm:ss");
            console.log(time_index);
            (type === "active_from_time" ? temp[index].time_slots[time_index].active_from_time = value : temp[index].time_slots[time_index].active_to_time = value);
        }
        console.log(temp);
    }
    const handleRemoveClick = index => {
        const list = [...hotlines];
        list.splice(index, 1);
        setHotlines(list);
    };
    return !props.show ? null : (
        <Modal className='hotline-modal' show={props.show} onHide={props.hide}>
            { !loading
                ? <Modal.Body>
                    <span className='strong'>Add Hotline</span>
                    <Form onSubmit={(event) => passDataToParent(event)}>
                        <Row>
                            <Col sm={12} md={8}>
                                <Form.Group as={Row} className='mt-3'>
                                    <Form.Label column sm="3">Hotline Name</Form.Label>
                                    <Col sm="7">
                                        <Form.Control type='text' name="contact_name" required defaultValue={props.editData && props.editData.category.contacts.length > 0 && props.editData.category.contacts[0].contact_name }/>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className='mt-3'>
                                    <Form.Label column sm="3">Hotline Number</Form.Label>
                                    <Col sm="7">
                                        <Form.Control 
                                            type='number' 
                                            name="contact_number" 
                                            required 
                                            defaultValue={props.editData && props.editData.category.contacts.length > 0 && props.editData.category.contacts[0].contact_number }
                                            disabled={props.editData}/>
                                    </Col>
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={4}>
                                <Form.Group as={Row} className='mt-3'>
                                    <Form.Label column sm="3">Priority</Form.Label>
                                    <Col sm="7">
                                        <Form.Control 
                                            as="select"
                                            name="priority" 
                                            required 
                                            defaultValue={props.editData && props.editData.category.contacts.length > 0 && props.editData.category.contacts[0].priority }
                                            >
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                        </Form.Control> 
                                    </Col>
                                </Form.Group>
                            </Col>
                        </Row>
                        <span className='text-secondary fs-13'>NOTE : Time format is in 24hr format</span>
                        {hotlineEmptyError && <span className='text-danger fs-13 mt-2 mb-2'>Please enter atleast 1 contact</span>}
                        {hotlines.map((item, index) => {
                            return <div className='hotline-card p-3' key={`hotline-card-${index}`}>
                                <span className='text-secondary fs-13'>Select Day(s)</span>
                                <div className='d-flex mt-3 justify-content-center align-items-center'>
                                    { Object.keys(days).map(day => {
                                        return (
                                            <span key={`${index}-day-${days[day]}`}>
                                                <input id={`${index}-day-${days[day]}`} type="checkbox" defaultChecked={hotlines[index].days.some(el => el === day)}  className='day-checkbox' value={day} onChange={(e) => handleInputChange(e, "checkbox", index)}/>
                                                <label htmlFor={`${index}-day-${days[day]}`} className='day-checkbox-label'>{days[day]}</label>
                                            </span>        
                                        )
                                    })}
                                </div>
                                { item.time_slots.map((time, time_index) => {
                                    return (
                                        <div  key={`time-slot-${time_index}`}>
                                            <Row className='mt-4'>
                                                <Col sm={6} md={6}>
                                                    <div className='p-1 h-100 d-flex justify-content-end align-items-center'>
                                                        <TimePicker
                                                            popupClassName='timepicker'
                                                            placement="topRight"
                                                            onChange={(e) => handleInputChange(e, "active_from_time", index, time_index)}
                                                            placeholder={time.active_from_time ? time.active_from_time : "Opens"}
                                                            name='active_from_time'
                                                            defaultValue={time.active_from_time}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col sm={6} md={6}>
                                                    <div className='p-1 h-100 d-flex justify-content-start align-items-center'>
                                                        <TimePicker
                                                            popupClassName='timepicker'
                                                            placement="topRight"
                                                            onChange={(e) => handleInputChange(e, "active_to_time", index, time_index)}
                                                            placeholder={time.active_to_time ? time.active_to_time : "Closes" }
                                                            name='active_to_time'
                                                            defaultValue={time.active_to_time}
                                                        />
                                                    <Button variant='light' size='sm' className='text-danger ml-4' onClick={() => removeTimeSlot(index, time_index)}><FiX strokeWidth='3' /></Button>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    )
                                })
                                }
                                <div className='d-flex justify-content-center align-items-center mt-3'>
                                    <Button variant='light' onClick={() => addTimeSlot(index)}>Add time slot</Button>
                                </div>
                            </div>
                        })}
                        <div className='d-flex align-items-center justify-content-start'>
                            <Button variant='primary' size='xs' onClick={() => addTiming()} className='d-flex align-items-center justify-content-center'><FiPlus className='mr-1' strokeWidth="3" />Add another</Button>
                        </div>
                        <div className='mt-5'>
                            <Button variant="light" onClick={props.hide}>Cancel</Button>
                            <Button className='ml-2' variant="success" type='submit' disabled={saveLoader}>
                                {saveLoader
                                    ? <><Spinner variant='light' size='sm' className='mr-2' /> Saving..</>
                                    : <>Save</>
                                }
                            </Button>
                        </div>
                    </Form>
                </Modal.Body>
                : <Loader />
            }
        </Modal>
    );
}