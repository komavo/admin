import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import './index.scss';

export default function DeleteConfirm(props) {
    return (
        <Modal className='delete-confirm' show={props.show ? true : false} onHide={props.hide}>
            <Modal.Body>Are you sure you want to delete?</Modal.Body>
            <Modal.Footer>
            <Button variant="light" onClick={props.hide}>
                Cancel
            </Button>
            <Button variant="danger" onClick={props.confirm} disabled={props.deleting}>
                {props.deleting ? 'Deleting...' : 'Delete'}
            </Button>
            </Modal.Footer>
        </Modal>
    );
}