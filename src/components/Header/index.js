import React, { useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Dropdown, Button } from 'react-bootstrap';
import axiosInstance from '../../utils/axiosAPI';
import * as actionTypes from '../../store/actions';
import './index.scss';
import { FiChevronDown, FiChevronLeft, FiBell } from 'react-icons/fi';


const Header = (props) => {
  const history = useHistory();
  const [newNotifications, setNewNotifications] = React.useState();

  const handleLogout = () => {
    axiosInstance.get('/logout/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        console.log(res.data);
      })
    props.destroyTokens();
    history.push('/login');
  }
  const handleChangePassword = () => {
    history.push('/change-password');
  }
  const redirectPostNotifications = () =>{
    history.push("/notifications");
  }
  return (
    <header className="topbar">
      <nav className="navbar top-navbar navbar-expand-md navbar-light">
        <Container fluid={true}>
          <div className=''>
            <Button variant='secondary' onClick={() => { history.goBack() }}><FiChevronLeft size='20' />Back</Button>
          </div>

          <div className='ml-auto d-flex align-items-center justify-content-center'>
            <Button variant="light" className='mr-2' onClick={() => redirectPostNotifications()}><FiBell size="20" />{newNotifications && newNotifications > 0 ? <span className='notification-indicator'>*</span> : null} </Button>
            &nbsp;&nbsp;
            <Dropdown className='user-dropdown'>
              <Dropdown.Toggle id="dropdown-basic">
                {props.user.username} <FiChevronDown c1lassName='ml-2' size="20"/>
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item className='topbar-dropdown-item' onClick={() => handleChangePassword()}>Change Password</Dropdown.Item>
                <Dropdown.Item className='topbar-dropdown-item' onClick={() => handleLogout()}>Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </Container>
      </nav>
    </header>
  )
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user,
    post_notifications : state.post_notifications
  }
};

const mapDispatchToProps = dispatch => {
  return {
    destroyTokens: () => dispatch({ type: actionTypes.DESTROY_TOKENS }),
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));