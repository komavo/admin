import React, {useEffect, useState} from 'react';
import { withRouter, Link} from 'react-router-dom';
import { FiChevronRight } from 'react-icons/fi';

import Logo from "../../assets/images/logo.png";

import './index.scss';

const TryHeader = (props)  => {
  const [title, setTitle] = useState("");
  useEffect(() => {
    setTitle(props.title ? props.title : "");
  }, [props.title]);

  return (
    <header className="topbar pt-2 pb-2">
      <div className="container-fluid">
        <nav className="navbar top-navbar navbar-expand-md navbar-dark">

          <div className="navbar-header col-xl-5 px-2 d-flex align-items-center justify-content-start">
            <img src={Logo} alt="logo" className="sidebar-icon" /> <span className="fs-24">Pizza Image</span> <FiChevronRight strokeWidth="3" className="mx-2 fs-16" /> <span className="strong h5 title mb-0">{title}</span>
          </div>

          <div className="nav ml-auto">
            <Link className="btn btn-primary px-4 py-2" to="/auth/signup">Sign Up</Link>
          </div>
        </nav>
      </div>
    </header>
  )
};

export default withRouter(TryHeader);