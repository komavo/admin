import * as actionTypes from "../actions";

const initialState = {
  isAuthenticated: false,
  user: {
    username: null,
    access_token: null,
  },
  post_notifications: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_TOKENS:
      return {
        ...state,
        user: {...action.payload},
        isAuthenticated: true
      };
    case actionTypes.DESTROY_TOKENS:
      return{
        ...state,
        user: {
          username: null,
          access_token : null,
        },
        isAuthenticated: false
      };
    case actionTypes.UPDATE_POST_NOTIFICATIONS:
      return{
        ...state,
        post_notifications: action.payload
      }
    default:
      return state;
  }
};

export default reducer;