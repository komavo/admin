import React, { useState, useEffect } from 'react';
import Aux from '../../hoc/_Aux';
import { Button, Spinner, Container, Row, Col, Form, Alert} from 'react-bootstrap';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Loader from '../../components/Loader';
import axiosInstance from '../../utils/axiosAPI';

const ChangePassword = (props) => {
  const [loading, setLoading] = useState(false);
  const [btnLoader, setBtnLoader] = useState(false);
  const [oldPasswordWrong, setOldPasswordWrong] = useState(false);
  const [passwordChanged, setPasswordChanged] = useState(false);
  const history = useHistory();

  const handleFormSubmit = (event) => {
    event.preventDefault();
    console.log(event.target.old_password.value);
    let formData = {
      old_password : event.target.old_password.value,
      new_password : event.target.password1.value
    }
    axiosInstance.post('/change-password/', formData, { headers: { 'Authorization': 'Bearer ' + props.user.access_token } })
      .then(res => {
        console.log(res.data);
        if(res.data === 'old-password-wrong'){
          setOldPasswordWrong(true);
          setPasswordChanged(false);
        }
        else if(res.data === 'password-changed'){
          setPasswordChanged(true);
          setOldPasswordWrong(false);
        }
        setBtnLoader(false);
        // history.push('/resources');
      })
  }
  return loading ? <div className='h-100 d-flex justiy-content-center align-items-center'> <Loader /></div> : (
    <Aux>
      <Container className='m-5'>
        <Form className='bg-white p-5' onSubmit={(event) => { setBtnLoader(true); handleFormSubmit(event) }}>
          { oldPasswordWrong && <Alert variant='warning'>Old password did not match with our records</Alert>}
          { passwordChanged && <Alert variant='success'>Your password has been changed !</Alert>}
          <Form.Group as={Row}>
            <Form.Label column sm={2}>Old Password*</Form.Label>
            <Col sm={10}><Form.Control type="password" name='old_password' className='mw-25' required /></Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={2}>New Password*</Form.Label>
            <Col sm={10}><Form.Control type="password" name='password1' className='mw-25' required /></Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={2}>Confirm Password*</Form.Label>
            <Col sm={10}><Form.Control type="password" name='password2' className='mw-25' required /></Col>
          </Form.Group>
          <Button variant='success' disabled={btnLoader} size='sm' className='mt-5' type='submit'>
            {btnLoader
              ? <><Spinner animation="border" variant='light' size='sm' /> Please wait... </>
              : <>Change Password</>
            }
          </Button>
        </Form>
      </Container>
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ChangePassword));