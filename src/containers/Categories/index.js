import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Modal, Button, Form, Row, Col, FloatingLabel, Spinner } from 'react-bootstrap';
import { FiPlus, FiTrash2, FiEdit2, FiUploadCloud, FiCheck } from 'react-icons/fi';
import Switch from "react-switch";
import Aux from '../../hoc/_Aux';
import BreadCrumb from '../../components/BreadCrumb';
import Loader from '../../components/Loader';
import axiosInstance from "../../utils/axiosAPI.js";
import imageToBase64 from 'image-to-base64/browser';
import "./index.scss";

const Categories = (props) => {
    const history = useHistory();

    const [loading, setLoading] = useState(false);
    const [breadCrumbs, setBreadCrumbs] = useState();
    const [menuCategories, setMenuCategories] = useState();
    const [menuCategoriesOriginal, setMenuCategoriesOriginal] = useState();
    const [btnLoading, setBtnLoading] = useState(false);
    const [uploadedImage, setUploadedImage] = useState();
    const [editCat, setEditCat] = useState();
    const [editCatModal, setEditCatModal] = useState();
    const [invalidName, setInvalidName] = useState();

    const setData = (items) =>{
        setMenuCategories(items);
        setMenuCategoriesOriginal(items);
    }

    const addCategory = () => {
        let temp = {
            new_cat: true,
            section_name: "",
            is_visible: false
        }
        setEditCat(temp);
        setEditCatModal(true);
    }

    const handleOpen = (item) => {
        console.log(item);
        setEditCat(item);
        setEditCatModal(true);
    }

    const handleClose = () => {
        setEditCatModal(false);
        setEditCat(null);
        setUploadedImage(null);
    }

    const handleUploadImage = (event) => {
        event.preventDefault();
        setUploadedImage(event.target.files[0]);
    }

    const handleEditChange = (e, type) => {
        var temp = { ...editCat };
        if (type === 'section_name') {
            temp.section_name = e.target.value;
        }
        setEditCat({ ...temp });
    }

    const deleteCategory = (item) => {
        axiosInstance.delete(`/admin/menu-section/${item.id}`, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
            .then(res => {
                var temp = [...menuCategories];
                var newTemp = temp.filter((i) => i.id != item.id);
                setData([...newTemp])
            })
            .catch(err => {
                console.log(err);
            })
    }

    const toBase64 = (image) => {
        return new Promise((resolve, reject) => {
            imageToBase64(URL.createObjectURL(image))
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => reject(err));
        })
    }

    const handleSubmit = async () => {
        console.log(editCat);
        let data = { ...editCat };
        if (data.section_name === "") {
            setInvalidName(true);
            return false;
        }
        setBtnLoading(true);
        if (uploadedImage) {
            data.image_mime = uploadedImage.type;
            data.image_base64 = await toBase64(uploadedImage);
        }
        if (data.section === 'no-section') data.section = null;
        console.log(uploadedImage);
        console.log(data);
        if (data.new_cat) {
            axiosInstance.post('/admin/menu-section', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...menuCategories];
                    temp.push(res.data.data)
                    setData([...temp]);
                    setInvalidName(false);
                    setUploadedImage(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => {
                    console.log(err)
                });
        }
        else {
            axiosInstance.patch('/admin/menu-section', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...menuCategories];
                    var index = temp.findIndex(item => item.id === res.data.data.id);
                    temp[index] = res.data.data;
                    setData([...temp])
                    setInvalidName(false);
                    setUploadedImage(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => {
                    console.log(err)
                });
        }
    }

    const updateVisibility = (item) => {
        let data = {
            id: item.id,
            is_visible: !item.is_visible
        }
        axiosInstance.patch('/admin/menu-section', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
            .then((res) => {
                console.log(res.data);
                var temp = [...menuCategories];
                var index = temp.findIndex(item => item.id === res.data.data.id);
                temp[index] = res.data.data;
                setData([...temp])
                setInvalidName(false);
                setUploadedImage(null);
                handleClose();
                setBtnLoading(false);
            })
            .catch((err) => {
                console.log(err)
            });
    }

    const handleSearch = (e) => {
        // if (e.target.value.length === 0){
        //     setData(se)
        // }
        var categories = [...menuCategoriesOriginal];
        var filteredItems = categories.filter(item => item.section_name.toLowerCase().search(e.target.value.toLowerCase()) !== -1);
        setMenuCategories([...filteredItems]);
    }

    const fetchMenuCategories = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/menu-section', { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
                .then(res => {
                    console.log(res.data.data);
                    setData(res.data.data);
                    resolve()
                })
                .catch(err => {
                    reject();
                })
        })
    }

    useEffect(() => {
        document.title = 'Sellers | Komavo Admin';
        setBreadCrumbs([
            { location: "Dashboard", path: '/dashboard' },
            { location: "Sellers", path: '/sellers' }
        ]);
        (async () => {
            try {
                await fetchMenuCategories();
            } catch (e) {
                console.log(e);
            } finally {
                setLoading(false);
            }
        })();
    }, []);

    return loading ? <Loader /> : (
        <Aux>
            <Container className="my-2">
                <BreadCrumb list={breadCrumbs} />
                <div className='d-flex justify-content-center align-items-center'>
                    <div className='text-secondary fs-25 flex-grow-1'>Categories</div>
                    <Button variant='primary' onClick={() => addCategory()}><FiPlus size="15" /> Add Category</Button>
                </div>
                <Form className='mt-4'>
                    <Col sm={6}>
                        <Form.Control type="text" name='search' placeholder='Search categories' className='mw-100' onChange={(e) => handleSearch(e)} />
                    </Col>
                </Form>
                <div className='mt-3'>
                    {menuCategories &&
                        <Row>{menuCategories.map((item, index) => {
                            return (
                                <Col key={`menu-category=${index}`} sm={3}>
                                    <div className='category-container'>
                                        {item.image
                                            ? <div className='d-flex align-items-center justify-content-end mw-100'><img src={`${process.env.REACT_APP_S3_PREFIX}${item.image}`} className='mw-100' style={{ height: 250, width: '100%' }} /></div>
                                            : <div style={{ height: 250, border: '2px dashed #444', borderRadius: '10px' }}></div>
                                        }
                                        <div className='flex-grow-1 fs-20'>{item.section_name}</div>
                                        <div className='d-flex justify-content-start align-items-center mt-2'>
                                            <div className='flex-grow-1 fs-13'>Visible to customers</div>
                                            <Switch
                                                onChange={() => updateVisibility(item)}
                                                checked={item.is_visible}
                                                height={20}
                                                width={40}
                                            />
                                        </div>

                                        <div className='mt-4 d-flex justify-content-end align-items-center mt-2'>
                                            <Button variant='dark' onClick={() => deleteCategory(item)} style={{ color: "#fa4b4b" }}><FiTrash2 color="#fa4b4b" size={15} />&nbsp;&nbsp;Delete</Button>
                                            <div className='px-2'></div>
                                            <Button variant='dark' onClick={() => handleOpen(item)} style={{ color: "#188DF4" }}><FiEdit2 color="#188DF4" size={15} /> &nbsp;&nbsp;Edit </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })}
                        </Row>
                    }
                </div>
            </Container>


            {editCat &&
                <Modal show={editCatModal} onHide={handleClose} backdrop="static" className='edit-item-modal'>
                    <Modal.Body>
                        <div className='fs-20'>{editCat && editCat.new_cat ? 'New ' : 'Edit '}Category</div>
                        {/* ITEM IMAGE */}
                        <Row>
                            <Col sm={4}>
                                <div className='mt-5'>
                                    {editCat.image && !uploadedImage
                                        ? <img src={`${process.env.REACT_APP_S3_PREFIX}${editCat.image}`} style={{ height: 200, width: 200 }} />
                                        :
                                        uploadedImage
                                            ? <><img src={URL.createObjectURL(uploadedImage)} style={{ height: 200, width: 200 }} /></>
                                            : <div style={{ height: 200, width: 200, border: '3px dashed #666', borderRadius: 20 }} className='flex-column d-flex align-items-center justify-content-center'>
                                                No Image
                                            </div>
                                    }
                                    <Form.Group className='mt-2'>
                                        <Form.Label htmlFor='upload-image' className='custom-upload-btn'>Upload Image <FiUploadCloud size='20' /></Form.Label>
                                        <Form.Control id='upload-image' type='file' hidden accept="image/*" onChange={(event) => handleUploadImage(event)} />
                                    </Form.Group>
                                </div>
                            </Col>
                            <Col sm={8}>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    <Col sm={8}>
                                        {invalidName && <span className='text-danger'>Required*</span>}
                                        <FloatingLabel label='Category Name'>
                                            <Form.Control type="text" name='section_name' defaultValue={editCat && editCat.section_name} className='mw-100' required onChange={(e) => handleEditChange(e, 'section_name')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>

                                <div className='mt-3 d-flex justify-content-start align-items-center' >
                                    <Button variant="secondary" onClick={handleClose}>
                                        Cancel
                                    </Button>
                                    <Button disabled={btnLoading} variant="success" onClick={() => handleSubmit()} style={{ marginLeft: '20px' }}>
                                        {btnLoading
                                            ? <>Saving Changes ... <Spinner animation='border' variant='light' /></>
                                            : <>Save Changes <FiCheck size='15' style={{ marginLeft: 7 }} /></>
                                        }

                                    </Button>
                                </div>
                            </Col>
                        </Row>

                    </Modal.Body>
                </Modal>
            }
        </Aux>
    );
};


const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Categories));