import React, { useState, useEffect } from "react";
import { useHistory, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Aux from "../../hoc/_Aux";
import {
  Container,
  Form,
  Button,
  ListGroup,
  Row,
  Col,
  Card,
} from "react-bootstrap";
import BreadCrumb from "../../components/BreadCrumb";
import Loader from "../../components/Loader";
import axiosInstance from "../../utils/axiosAPI";
import './index.scss';
import moment from "moment";

const Notification = (props) => {
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [posts, setPosts] = useState();
  const [comments, setComments] = useState();
  const [replies, setReplies] = useState();
  const [breadCrumbs, setBreadCrumbs] = useState();
  
  const fetchNotifications = () =>{
    return new Promise((resolve, reject) => {
      axiosInstance.get('/get-notifications/', { headers: { Authorization: 'Bearer ' + props.user.access_token }, params : { type : 'detail'} })
      .then(res =>{
        console.log(res.data);
        setPosts(res.data.posts);
        setComments(res.data.comments);
        setReplies(res.data.replies);
        resolve();
      })
      .catch(err =>{
        reject();
      })
    })
  }

  useEffect(() => {
    document.title = "Notifications | Diall Admin";
    setBreadCrumbs([
      { location: "Dashboard", path: "/dashboard" },
      { location: "Notification", path: "/notification" },
    ]);
    (async () => {
      try{
        await fetchNotifications();
      }
      catch (e){
        console.log(e);
      }
      finally{
        setLoading(false)
      }
    })();
  }, []);

  return loading ? <div className='d-flex justify-content-center align-items-center h-100'><Loader /></div> : (
    <Aux>
      <Container fluid className="mt-5 mx-auto mb-5">
      <BreadCrumb list={breadCrumbs} />
      <Row>
        <Col sm={12} md={6}>
          <span className='text-secondary strong fs-14'>NEW POSTS</span>
          {posts && posts.length > 0 
            ? <>
            {posts.map((item, index) => {
              return (
                <div key={`post-${index}`} className='post-card'  onClick={() => history.push(`/posts/details/${item.post_id}`)}>
                  <span className='post-card-content link'>{item.post_content}</span>
                  <span className='text-secondary fs-13'>{item.post_title} | {moment(item.created_at).format('MMMM Do, YYYY')}</span>
                </div>
              )
            })}
            </>
            : <div className='text-secondary fs-13'>No new posts</div>
          }
        </Col>
        <Col sm={12} md={3}>
          <span className='text-secondary strong fs-14'>NEW COMMENTS</span>
          {comments && comments.length > 0 
            ? <>
            {comments.map((item, index) => {
              return (
                <div key={`comment-${index}`} className='post-card' onClick={() => history.push(`/posts/details/${item.post.post_id}`)}>
                  <span className='fs-13'>{item.post_comment_title} <span className='text-secondary'>commented</span></span>
                  <span className='post-card-content link'>{item.post_comment_content}</span>
                  <span className='text-secondary fs-13'>{moment(item.created_at).format('MMMM Do, YYYY')}</span>
                </div>
              )
            })}
            </>
            : <div className='text-secondary fs-13'>No new comments</div>
          }
        </Col>
        <Col sm={12} md={3}>
          <span className='text-secondary strong fs-14'>NEW COMMENT REPLIES</span>
          {replies && replies.length > 0 
            ? <>
            {replies.map((item, index) => {
              return (
                <div key={`post-${index}`} className='post-card'  onClick={() => history.push(`/posts/details/${item.comment.post}`)}>
                <span className='fs-13'>{item.reply_title} <span className='text-secondary'>replied to</span> {item.reply_person}</span>
                  <span className='post-card-content link'>{item.reply_content}</span>
                  <span className='text-secondary fs-13'>{moment(item.created_at).format('MMMM Do, YYYY')}</span>
                </div>
              )
            })}
            </>
            : <div className='text-secondary fs-13'>No new comment replies</div>
          }
        </Col>
      </Row>
      </Container>
    </Aux>
  );
};


const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user,
    post_notifications : state.post_notifications
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Notification));