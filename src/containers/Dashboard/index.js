import React, { useState, useEffect } from 'react';
import Aux from '../../hoc/_Aux';
import { Container, Row, Col, Table } from 'react-bootstrap';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Loader from '../../components/Loader';
import './index.scss';
import axiosInstance from '../../utils/axiosAPI';
import * as actionTypes from '../../store/actions';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer, Tooltip } from 'recharts';

const Dashboard = (props) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState();

  const fetchDashboardData = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/admin/dashboard', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          console.log(res.data.data);
          setData(res.data.data);
          resolve()
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    document.title = 'Dashboard | Komavo Admin';
    (async () => {
      try {
        await fetchDashboardData();
        setInterval(fetchDashboardData, 60000)
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);


  return loading ? <div className='h-100 d-flex justiy-content-center align-items-center'> <Loader /></div> : (
    <Aux>
      <Container className='my-3'>
        <div className='text-secondary fs-25'>Dashboard</div>

        <Row className='my-3'>
          <Col>
            <div className='total-data-container'>
              <div className='total-data'>{data.total_orders}</div>
              <div className='total-data-label'>Total Orders</div>
            </div>
          </Col>
          <Col>
            <div className='total-data-container'>
              <div className='total-data'>{data.total_sellers}</div>
              <div className='total-data-label'>Total Sellers</div>
            </div>
          </Col>
          <Col>
            <div className='total-data-container'>
              <div className='total-data'>{data.total_customers}</div>
              <div className='total-data-label'>Total Customers</div>
            </div>
          </Col>
        </Row>
        <div className='my-4   chart-container'>
          <ResponsiveContainer width="100%" height={400}>
            <LineChart data={data.orders_chart_data}  margin={{ top: 5, right: 20, left: 20, bottom: 5 }}>
              <Line type="monotone" dataKey="orders" stroke="#fa4b4b" />
              <CartesianGrid stroke="#333" strokeDasharray='5 5' />
              <XAxis dataKey="day" />
              <YAxis />
              <Tooltip />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </Container>
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateNotifications: (payload) => dispatch({ type: actionTypes.UPDATE_POST_NOTIFICATIONS, payload: payload })
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));