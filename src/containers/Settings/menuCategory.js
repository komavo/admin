import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import './index.scss';
import { Container, Tab, Row, Col, Nav, Form, FloatingLabel, Button } from 'react-bootstrap';
import axiosInstance from "../../utils/axiosAPI.js";
import { FiTrash2, FiPlus } from 'react-icons/fi';


const MenuCategory = (props) => {
    const history = useHistory();

    const [loading, setLoading] = useState(false);
    const [breadCrumbs, setBreadCrumbs] = useState();
    const [menuCategories, setMenuCategories] = useState();

    const addCategory = (e) => {
        e.preventDefault();
        var data = {
            section_name: e.target.category.value
        }
        axiosInstance.post('/admin/menu-section/add', { ...data }, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
            .then(res => {
                var temp = menuCategories;
                temp.push({ ...res.data.data });
                setMenuCategories([...temp]);
            })
            .catch(err => {
                console.log(err);
            })
    }
    const deleteCategory = (item) => {
        axiosInstance.delete(`/admin/menu-section/${item.id}`, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
            .then(res => {
                var temp = [...menuCategories];
                var newTemp = temp.filter((i) => i.id != item.id);
                setMenuCategories([...newTemp]);
            })
            .catch(err => {
                console.log(err);
            })
    }
    const fetchMenuCategories = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/menu-section', { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
                .then(res => {
                    setMenuCategories(res.data.data);
                    resolve()
                })
                .catch(err => {
                    reject();
                })
        })
    }

    useEffect(() => {
        document.title = 'Sellers | Komavo Admin';
        setBreadCrumbs([
            { location: "Dashboard", path: '/dashboard' },
            { location: "Sellers", path: '/sellers' }
        ]);
        (async () => {
            try {
                await fetchMenuCategories();
            } catch (e) {
                console.log(e);
            } finally {
                setLoading(false);
            }
        })();
    }, []);
    return loading ? <Loader /> : (
        <Aux>
            <div>
                <Form onSubmit={(e) => addCategory(e)}>
                    <Form.Group as={Row} className='form-group mt-4 mb-5 '>
                        {/* <Form.Label column sm={2}>Store Name</Form.Label> */}
                        <Col sm={8}>
                            <FloatingLabel label='Category Name'>
                                <Form.Control type="text" name='category' className='mw-100' id="section-name" />
                            </FloatingLabel>
                        </Col>
                        <Col sm={4}>
                            <div className='h-100 d-flex align-items-center justify-content-center'>
                                <Button variant='primary' type='submit'><FiPlus size="15" strokeWidth="4" /> Add Category</Button>
                            </div>
                        </Col>
                    </Form.Group>
                </Form>
                {menuCategories &&
                    <Row>{menuCategories.map((item, index) => {
                        return (
                            <Col key={`menu-category=${index}`} sm={3}>
                                <div className='menu-category-item'>
                                    <div className='d-flex justify-content-start align-items-center'>
                                        <div className='flex-grow-1'>{item.section_name}</div>
                                        <div><FiTrash2 color="red" style={{ cursor: "pointer" }} onClick={() => deleteCategory(item)} /></div>
                                    </div>
                                </div>
                            </Col>
                        );
                    })}
                    </Row>
                }
            </div>
        </Aux>
    );
};


const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MenuCategory));