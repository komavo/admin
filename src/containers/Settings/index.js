import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Aux from '../../hoc/_Aux';
import BreadCrumb from '../../components/BreadCrumb';
import { Container, Tab, Row, Col, Nav } from 'react-bootstrap';
import './index.scss';
import MenuCategory from './menuCategory';

const Settings = () => {
  const history = useHistory();

  const [breadCrumbs, setBreadCrumbs] = useState();


  useEffect(() => {
    document.title = 'Settings | Komavo Admin';
    setBreadCrumbs([
      { location: "Dashboard", path: '/dashboard' },
      { location: "Settings", path: '/settings' }
    ]);
  }, []);

  return (
    <Aux>
      <Container fluid={true} className='my-2'>
        <BreadCrumb list={breadCrumbs} />
        <div className='text-secondary fs-25'>Settings</div>
        <Tab.Container className='mt-5' id="custom-tabs" defaultActiveKey="menu-category">
          <Row className='custom-tabs mt-2 p-2'>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column">
                <Nav.Item>
                  <Nav.Link eventKey="menu-category">Menu Categories</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="secret">Secret</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={9}>
              <Tab.Content>
                <Tab.Pane eventKey="menu-category">
                  <MenuCategory />
                </Tab.Pane>
                <Tab.Pane eventKey="secret">
                  <div className=''>Jyada adventure mat karo, apna kaam karo :D</div>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Container>
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Settings));