import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Row, Col, Form, Button, Alert, Spinner, Badge, ProgressBar, Modal, FloatingLabel, InputGroup, FormControl } from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import axiosInstance from '../../utils/axiosAPI';
import AWS from 'aws-sdk';
import './index.scss';
import { FiPlus, FiCheck, FiTrash2, FiUploadCloud } from 'react-icons/fi';
import imageToBase64 from 'image-to-base64/browser';
import Switch from "react-switch";

AWS.config.update({
    accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY
});
const s3 = new AWS.S3({
    params: { Bucket: process.env.REACT_APP_AWS_STORAGE_BUCKET_NAME },
    region: process.env.REACT_APP_AWS_S3_REGION_NAME
})

function SellerMenu(props) {
    const history = useHistory();

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [seller, setSeller] = useState();
    const [menu, setMenu] = useState([]);
    const [campaign, setCampaign] = useState([]);
    const [oriMenu, setOriMenu] = useState([]);
    const [menuSection, setMenuSection] = useState();
    const [invalidName, setInvalidName] = useState(false);
    const [invalidPrice, setInvalidPrice] = useState(false);

    const [uploadedImage, setUploadedImage] = useState(null);

    const [editItem, setEditItem] = useState(null);
    const [editModal, setEditModal] = useState(false);
    const handleClose = () => {
        setEditModal(false);
        setEditItem(null);
        setUploadedImage(null);
        // window.location.reload();
    }
    const handleOpen = (item) => {
        console.log(item);
        setEditItem(item);
        setEditModal(true);
    }

    const addNewItem = () => {
        var newItem = {
            new_item: true,
            business_id: seller.id,
            image: null,
            name: "",
            price: 0,
            is_veg: true,
            section: null
        }
        setEditItem(newItem);
        setEditModal(true);
    }

    const handleDelete = () => {
        axiosInstance.delete(`/admin/sellers/menu/delete/${editItem.id}`, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
            .then((res) => {
                console.log(res.data);
                var temp = [...menu];
                var newTemp = temp.filter(item => item.id != editItem.id);
                setMenu([...newTemp]);
                setOriMenu([...newTemp]);
                handleClose();
            })
            .catch((err) => console.log(err));
    }
    const handleUploadImage = (event) => {
        event.preventDefault();
        setUploadedImage(event.target.files[0]);
    }
    const handleEditChange = (e, type) => {
        var temp = { ...editItem };
        if (type === 'section') {
            var section = e.target.value;
            console.log(section);
            if (section === 'No Section') temp.section = null;
            else temp.section = section;
        }
        if (type === 'name') {
            temp.name = e.target.value;
        }
        if (type === 'price') {
            temp.price = parseInt(e.target.value);
        }
        if (type === 'is_veg') {
            temp.is_veg = e;
        }
        setEditItem({ ...temp });
    }

    const toBase64 = (image) => {
        return new Promise((resolve, reject) => {
            imageToBase64(URL.createObjectURL(image))
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => reject(err));
        })
    }
    const handleSubmit = async () => {
        let data = editItem;
        if (data.name === "") {
            setInvalidName(true);
            return false;
        }
        if (data.price <= 0) {
            setInvalidPrice(true);
            return false;
        }
        setBtnLoading(true);
        if (uploadedImage) {
            data.image_mime = uploadedImage.type;
            data.image_base64 = await toBase64(uploadedImage);
        }
        if (data.section === 'no-section') data.section = null;
        console.log(data);
        if (data.new_item) {
            axiosInstance.post('/admin/sellers/menu/add', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...menu];
                    temp.push(res.data.data)
                    setMenu([...temp]);
                    setOriMenu([...temp]);
                    setInvalidName(false);
                    setInvalidPrice(false);
                    setUploadedImage(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => {
                    console.log(err)
                }
                );
        }
        else {
            axiosInstance.post('/admin/sellers/menu/update', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...menu];
                    var index = temp.findIndex(item => item.id === res.data.data.id);
                    temp[index] = res.data.data;
                    setMenu([...temp]);
                    setOriMenu([...temp]);
                    setUploadedImage(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => console.log(err));
        }
    }
    const fetchMenuSections = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/menu-section', { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    setMenuSection(res.data.data);
                    resolve();
                })
                .catch((err) => {
                    console.log(err);
                    reject();
                })
        })
    }
    const fetchSellerMenu = (id) => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/sellers/menu/' + id, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then(res => {
                    setSeller(res.data.data.seller);
                    setMenu(res.data.data.menu);
                    setOriMenu(res.data.data.menu);
                    if (res.data.data.campaign.length === 0) {
                        setCampaign(null);
                    }
                    else
                        setCampaign(res.data.data.campaign[0])
                    resolve()
                })
                .catch(err => {
                    reject();
                })
        })
    }


    useEffect(() => {
        document.title = 'Menu List | Komavo Admin';
        setLoading(true);
        (async () => {
            try {
                await fetchSellerMenu(props.match.params.id);
                await fetchMenuSections();
            }
            catch (e) {
                console.log(e);
            }
            finally {
                setLoading(false);
            }
        })();
    }, []);

    const handleSearch = (e) => {
        var items = [...oriMenu];
        var filteredItems = items.filter(item => item.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1);
        setMenu([...filteredItems]);
    }

    const handleRemoveDiscount = (id) => {
        axiosInstance.delete(`/admin/sellers/menu/item/${id}/campaign`, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
            .then(res => {
                var item = { ...editItem };
                item.discount = 0;
                setEditItem({ ...item });
            })
            .catch(err => console.log(err));
        return;
    }
    return (loading ? <Loader />
        :
        <Aux>
            <Container className='p-5'>
                <div className='add-item-btn-container'>
                    <div className='btn btn-primary' onClick={() => addNewItem()}>
                        <FiPlus size='20' strokeWidth='4' /> Add New Item
                    </div>
                </div>
                <div className='text-secondary fs-25'>
                    {seller.store_name} | Menu
                    {menu && <div className='menu-count-pill' >{menu.length} Items</div>}
                    {/* <Form.Group as={Row} className='form-group mt-4'> */}
                    <Form className='mt-2'>
                        <Col sm={6}>
                            <Form.Control type="text" name='search' placeholder='Search items' className='mw-100' onChange={(e) => handleSearch(e)} style={{ padding: '15px 20px' }} />
                        </Col>
                    </Form>
                    {/* </Form.Group> */}
                </div>
                {menu &&
                    <Row>
                        {menu.map((item, index) => {
                            return (
                                <Col lg={3} md={6} key={`menu-${index}`} >
                                    <div className='mt-4 menu-item-container' onClick={() => handleOpen(item)}>
                                        {item.image
                                            ? <div className='mw-100'><img src={`${process.env.REACT_APP_S3_PREFIX}${item.image}`} className='mw-100' style={{ height: 250, width: '100%' }} /></div>
                                            : <div style={{ height: 250 }}></div>
                                        }
                                        <div className='text-light fs-20 mt-3'>{item.name}</div>
                                        <div><span className='text-light fs-15'>&#8377;</span><span className='text-light fs-20'>{item.price}</span></div>
                                        <Badge bg={item.is_veg ? 'success' : 'danger'}> {item.is_veg ? 'Veg' : 'Non-Veg'} </Badge>
                                        <Badge bg='light' style={{ color: '#111', marginLeft: 8, fontSize: 10 }}>{item.section ? item.section : ''}</Badge>
                                        {item.discount > 0 &&
                                            <div className='campaign-badge'>
                                                {campaign.name} - {item.discount}%
                                            </div>
                                        }

                                    </div>
                                </Col>
                            );
                        })}
                    </Row>
                }
                {!menu && <span className='text-light'>No items added yet</span>}
            </Container>


            {editItem &&
                <Modal show={editModal} onHide={handleClose} backdrop="static" className='edit-item-modal'>
                    <Modal.Body>
                        <div className='fs-20'>Edit Item</div>
                        {/* ITEM IMAGE */}
                        <Row>
                            <Col sm={6}>
                                <div className='mt-5 d-flex flex-column justify-content-end align-items-center'>
                                    {editItem.image && !uploadedImage
                                        ? <img src={`${process.env.REACT_APP_S3_PREFIX}${editItem.image}`} style={{ height: 200, width: 200 }} />
                                        :
                                        uploadedImage
                                            ? <><img src={URL.createObjectURL(uploadedImage)} style={{ height: 200, width: 200 }} /></>
                                            : <div style={{ height: 200, width: 200, border: '3px dashed #666', borderRadius: 20, backgroundColor: "#191A19" }} className='flex-column d-flex align-items-center justify-content-center'>
                                                No Image
                                            </div>
                                    }
                                    <Form.Group className='mt-2'>
                                        <Form.Label htmlFor='upload-image' className='custom-upload-btn'>Upload Image <FiUploadCloud size='20' /></Form.Label>
                                        {/* <Form.File id='thumbnail-image' hidden accept="image/*" onChange={(event) => handleThumbnailUpload(event)} /> */}
                                        <Form.Control id='upload-image' type='file' hidden accept="image/*" onChange={(event) => handleUploadImage(event)} />
                                    </Form.Group>
                                </div>
                            </Col>
                            <Col sm={6}>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    <Col sm={12}>
                                        <FloatingLabel controlId="floatingSelect" label="Category">
                                            <Form.Select aria-label="Floating label select example" name='section' defaultValue={editItem.section} onChange={(e) => handleEditChange(e, 'section')}>
                                                <option disabled>Open this select menu</option>
                                                <option value='no-section'>No Section</option>
                                                {menuSection && menuSection.map((item, index) => {
                                                    return (
                                                        <option key={`option-${index}`} value={item.section_name}>{item.section_name}</option>
                                                    );
                                                })}
                                            </Form.Select>
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    {/* <Form.Label column sm={2}>Store Name</Form.Label> */}
                                    <Col sm={12}>
                                        {invalidName && <span className='text-danger'>Required field</span>}
                                        <FloatingLabel label='Item Name'>
                                            <Form.Control type="text" name='name' defaultValue={editItem && editItem.name} className='mw-100' required onChange={(e) => handleEditChange(e, 'name')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    {/* <Form.Label column sm={2}>Store Name</Form.Label> */}
                                    <Col sm={12}>
                                        {invalidPrice && <span className='text-danger'>Required field</span>}
                                        <FloatingLabel label='Price'>
                                            <Form.Control type="text" name='price' defaultValue={editItem && editItem.price} className='mw-100' required onChange={(e) => handleEditChange(e, 'price')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>

                                <Col sm={8}>
                                    <div className='p-2 mt-2 d-flex align-items-center justify-content-start'>
                                        <div style={{ marginRight: 10 }}>
                                            Vegetarian
                                        </div>
                                        <Switch 
                                            onChange={(e) => handleEditChange(e, 'is_veg')} 
                                            checked={editItem.is_veg} 
                                            offColor='#fa4b4b'
                                            height={20}
                                            width={40}
                                        />
                                    </div>
                                </Col>

                                {editItem.discount > 0 &&
                                    <Col sm={8} className='mt-4'>
                                        <div className='d-flex justify-content-start align-items-center campaign-badge' style={{ backgroundColor: '#122222', boxShadow: '0px 0px 5px 2px #111' }}>
                                            <div className='flex-grow-1'>{campaign.name} {editItem.discount}%</div>
                                            <a><FiTrash2 color='#cd4b4b' onClick={() => handleRemoveDiscount(editItem.id)} /></a>
                                        </div>
                                    </Col>
                                }
                            </Col>
                        </Row>
                        <div className='mt-5 d-flex justify-content-end align-items-center' >
                            <Button variant="secondary" onClick={handleClose}>
                                Cancel
                            </Button>
                            <Button variant="primary" onClick={() => handleDelete()} style={{ marginLeft: '20px' }}>
                                Delete this item <FiTrash2 size='15' style={{ marginLeft: 7 }} />
                            </Button>
                            <Button disabled={btnLoading} variant="success" onClick={() => handleSubmit()} style={{ marginLeft: '20px' }}>
                                {btnLoading
                                    ? <>Saving Changes ... <Spinner animation='border' variant='light' /></>
                                    : <>Save Changes <FiCheck size='15' style={{ marginLeft: 7 }} /></>
                                }

                            </Button>
                        </div>
                    </Modal.Body>
                </Modal>
            }
        </Aux>
    );
};

const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SellerMenu));
