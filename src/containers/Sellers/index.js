import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Table, Badge, Button } from 'react-bootstrap';
import { FiCheck, FiX } from 'react-icons/fi';

import Aux from '../../hoc/_Aux';
import BreadCrumb from '../../components/BreadCrumb';
import Loader from '../../components/Loader';

import axiosInstance from "../../utils/axiosAPI.js";
import "./index.scss";

const Sellers = (props) => {
  const history = useHistory();

  const [loading, setLoading] = useState(true);
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [sellers, setSellers] = useState();


  const getDetails = (seller_id) =>{
    // console.log(seller_id);
    history.push(`/sellers/details/${seller_id}`);
  }

  const fetchSellers = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/admin/sellers', { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
        .then(res => {
          setSellers(res.data.data);
          resolve()
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    document.title = 'Sellers | Komavo Admin';
    setBreadCrumbs([
      { location: "Dashboard", path: '/dashboard' },
      { location: "Sellers", path: '/sellers' }
    ]);
    (async () => {
      try {
        await fetchSellers();
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const verifySeller = (id, status) => {
    let data = {
      business_id: id,
      status: status
    }
    axiosInstance.post('/admin/sellers/verify', {...data}, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
      .then(res => {
        window.location.reload();
      })
      .catch(err => {
      })
  }

  return (
    <Aux>
      <Container className='my-2'>
        <BreadCrumb list={breadCrumbs} />
        <div className='text-secondary fs-25'>Sellers</div>
        {loading
          ? <Loader />
          :
          <div className='mt-4'>
            <Table hover size='sm' >
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Store Name</th>
                  <th>Phone</th>
                  <th>First Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                { sellers && sellers.map((item, index) => {
                  return (
                    <tr key={`seller-${index}`}>
                      <td onClick={() => getDetails(item.id)}>{item.id}</td>
                      <td onClick={() => getDetails(item.id)}>{item.store_name}</td>
                      <td onClick={() => getDetails(item.id)}>{item.phone}</td>
                      <td onClick={() => getDetails(item.id)}>{item.first_name}</td>
                      <td onClick={() => getDetails(item.id)}>{item.is_open ? <Badge bg='success' size='md'>Open</Badge> : <Badge bg='danger'>Closed</Badge>}</td>
                      <td>
                      {item.is_verified 
                        ? <Badge bg='success'>Verified</Badge> 
                        : item.verification_status == "rejected" 
                          ? <Badge bg='danger'>Rejected</Badge> 
                          : <div className='d-flex justify-content-start align-items-center'>
                              <Button variant='success' size='sm' onClick={() => verifySeller(item.id, "verified")}><FiCheck size='13' strokeWidth='3'/> Accept</Button>
                              <div className='mx-2'></div>
                              <Button variant='danger' size='sm' onClick={() => verifySeller(item.id, "rejected")}><FiX size='13' strokeWidth='3'/> Reject</Button>
                            </div>
                      }
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        }
      </Container>
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sellers));