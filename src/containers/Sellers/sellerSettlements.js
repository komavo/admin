import React, { useState, useEffect } from "react";
import { withRouter, useHistory, Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  Container,
  Table,
  Button,
  Modal,
  Form,
  Row,
  Col,
  FloatingLabel,
  Spinner,
} from "react-bootstrap";
import { FiTrash, FiPlus, FiCheck, FiX } from "react-icons/fi";

import Aux from "../../hoc/_Aux";
import BreadCrumb from "../../components/BreadCrumb";
import Loader from "../../components/Loader";
import DeleteConfirm from "../../components/DeleteConfim";

import { apiCall } from "../../utils/services";
import axiosInstance from "../../utils/axiosAPI.js";
import "./index.scss";
import moment from 'moment';

const SellerSettlements = (props) => {
  const history = useHistory();

  const [loading, setLoading] = useState(true);
  const [btnLoading, setBtnLoading] = useState(false);
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [seller, setSeller] = useState();
  const [settlements, setSettlements] = useState();
  const [account, setAccount] = useState();

  const [amountError, setAmountError] = useState(null);
  const [createSettlementModal, setCreateSettlementModal] = useState(false);

  const handleClose = () => {
    setAmountError(null);
    setCreateSettlementModal(false);
  };
  const handleOpen = () => {
    setAmountError(null);
    setCreateSettlementModal(true);
  };
  const handleCreateSettlement = (e) => {
    e.preventDefault();
    setBtnLoading(true);
    let amount = e.target.amount.value;
    if (amount > account.balance) {
      setAmountError("The amount cannot be higher than current balance !");
      setBtnLoading(false);
      return false;
    }
    if (amount < 0) {
      setAmountError("The amount cannot be less than 0 !");
      setBtnLoading(false);
      return false;
    }
    if (amount == 0) {
      setAmountError("The amount cannot be 0");
      setBtnLoading(false);
      return false;
    }
    setAmountError(null);
    let data = {
      business_id: seller.id,
      amount: amount,
    };
    console.log(data)
    axiosInstance
      .post(
        `/admin/sellers/details/create-settlement`,
        { ...data },
        {
          headers: {
            Authorization: "Bearer " + props.user.access_token,
            "content-type": "application/json",
          },
        }
      )
      .then((res) => {
          window.location.reload();
        setBtnLoading(false);
        handleClose();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const fetchSettlements = () => {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get(`/admin/sellers/details/${props.match.params.id}/settlements`, {
          headers: {
            Authorization: "Bearer " + props.user.access_token,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          setSeller(res.data.data.seller);
          setAccount(res.data.data.account);
          setSettlements(res.data.data.settlements);
          setBreadCrumbs([
            { location: "Dashboard", path: "/dashboard" },
            { location: "Sellers", path: "/sellers" },
            {
              location: res.data.data.seller.store_name,
              path: `/sellers/details/${props.match.params.id}`,
            },
            {
              location: "Settlements",
              path: "#",
            },
          ]);
          resolve();
        })
        .catch((err) => {
          reject();
        });
    });
  };

  useEffect(() => {
    document.title = "Sellers | Komavo Admin";
    (async () => {
      try {
        await fetchSettlements();
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <Aux>
      <Container fluid={true} className="my-2">
        <BreadCrumb list={breadCrumbs} />
        <div className="mt-4 text-secondary fs-30">{seller.store_name}</div>
        <div
          className="text-secondary campaign-box fs-30"
          style={{ display: "inline-block" }}
        >
          Total Revenue <span className="text-danger fs-20">&#8377;</span>
          <span className="text-danger">{seller.total_revenue}</span>
        </div>
        <div
          className="text-secondary campaign-box fs-30 mx-3"
          style={{ display: "inline-block" }}
        >
          Current Balance <span className="text-danger fs-20">&#8377;</span>
          <span className="text-danger">{account && <>{account.balance}</>}</span>
        </div>

        <div className="mt-4 d-flex justify-content-start align-items-center">
          <div className="flex-grow-1 text-secondary fs-30">
            All Settlements
          </div>
          <Button variant="primary" onClick={() => handleOpen()}>
            <FiPlus strokeWidth="3" size="15" />
            Create Settlement
          </Button>
        </div>

        <div className="mt-2">
          {settlements && settlements.length > 0 ? (
            <Table hover size="sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Amount</th>
                  <th>Settled at</th>
                </tr>
              </thead>
              <tbody>
                {settlements.map((item, index) => {
                  return (
                    <tr key={`seller-${index}`}>
                      <td>{item.id}</td>
                      <td>{item.amount}</td>
                      <td>{moment(item.created_at).format('Do MMM, YY | h:mm')}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          ) : (
            <div className="text-secondary ">No settlements yet</div>
          )}
        </div>
      </Container>
      <Modal
        show={createSettlementModal}
        onHide={handleClose}
        backdrop="static"
        className="mt-5 edit-item-modal"
      >
        <Modal.Body>
          <div className="fs-20">Create Settlement</div>
          <Form onSubmit={(e) => handleCreateSettlement(e)}>
            <Form.Group as={Row} className="form-group mt-4">
              <Col sm={8}>
                {amountError && (
                  <span className="text-danger">{amountError}</span>
                )}
                <FloatingLabel label="Amount to be settled">
                  <Form.Control
                    type="number"
                    name="amount"
                    className="mw-100"
                    required
                  />
                </FloatingLabel>
              </Col>
            </Form.Group>
            <div className="mt-4 d-flex justify-content-start align-items-center">
              <Button variant="light" onClick={() => handleClose()}>
                Cancel
              </Button>
              <Button
                disabled={btnLoading}
                variant="primary"
                className="mx-3"
                type="submit"
              >
                {btnLoading ? (
                  <>
                    Settling the amount{" "}
                    <Spinner animation="border" variant="light" size="sm" />
                  </>
                ) : (
                  <>Settle Amount</>
                )}
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </Aux>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SellerSettlements)
);
