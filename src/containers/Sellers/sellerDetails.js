import React, { useState, useEffect } from "react";
import { withRouter, useHistory, Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Alert,
  Spinner,
  Modal,
  FloatingLabel,
} from "react-bootstrap";
import Aux from "../../hoc/_Aux";
import Loader from "../../components/Loader";
import axiosInstance from "../../utils/axiosAPI";
import { GoLocation } from "react-icons/go";
import "./index.scss";
import moment from "moment";
import { FiArrowRight, FiCheck, FiUploadCloud, FiPlus } from "react-icons/fi";
import imageToBase64 from "image-to-base64/browser";
import TodaySpecial from "./todays-special.js";

function SellerDetails(props) {
  const history = useHistory();

  const [loading, setLoading] = useState(true);
  const [saveLoader, setSaveLoader] = useState(false);
  const [seller, setSeller] = useState();
  const [campaigns, setCampaigns] = useState();
  const [uploadedImage, setUploadedImage] = useState(null);
  const [updatedAlert, setUpdatedAlert] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [campaignModal, setCampaginModal] = useState(null);
  const [campaignImage, setCampaignImage] = useState();

  const handleClose = () => {
    setBtnLoading(false);
    setCampaginModal(false);
  };
  const handleOpen = () => {
    setCampaginModal(true);
    setBtnLoading(false);
  };
  const handleUploadCampaignImage = (event) => {
    event.preventDefault();
    console.log("saved");
    setCampaignImage(event.target.files[0]);
  };
  const handleCreateCampaign = async (e) => {
    setBtnLoading(true);
    e.preventDefault();
    var data = {
      business_id: props.match.params.id,
      name: e.target.name.value,
      discount: e.target.discount.value,
    };
    if (campaignImage) {
      data.image_mime = campaignImage.type;
      data.image_base64 = await toBase64(campaignImage);
    }
    axiosInstance
      .post(
        "/admin/sellers/campaign",
        { ...data },
        {
          headers: {
            Authorization: "Bearer " + props.user.access_token,
            "content-type": "application/json",
          },
        }
      )
      .then((res) => {
        var temp = [...campaigns];
        temp.push(res.data.data);
        setCampaigns([...temp]);
        handleClose();
      })
      .catch((err) => console.log(err));
  };

  const endCampaign = (campaign_id) => {
    axiosInstance
      .delete("/admin/sellers/campaign/" + campaign_id, {
        headers: {
          Authorization: "Bearer " + props.user.access_token,
          "content-type": "application/json",
        },
      })
      .then((res) => {
        var temp = campaigns.filter((item) => item.id != res.data.data.id);
        setCampaigns([...temp]);
        handleClose();
      })
      .catch((err) => console.log(err));
  };

  const fetchSellerDetails = (id) => {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get("/admin/sellers/details/" + id, {
          headers: {
            Authorization: "Bearer " + props.user.access_token,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          setSeller(res.data.data);
          resolve();
        })
        .catch((err) => {
          reject();
        });
    });
  };

  const fetchCampaigns = (id) => {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get("/admin/sellers/campaign/" + id, {
          headers: {
            Authorization: "Bearer " + props.user.access_token,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          setCampaigns(res.data.data);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  const getMenu = () => {
    history.push(`/sellers/menu/${props.match.params.id}`);
  };

  useEffect(() => {
    document.title = "Seller Details | Komavo Admin";
    setLoading(true);
    (async () => {
      try {
        await fetchSellerDetails(props.match.params.id);
        await fetchCampaigns(props.match.params.id);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const toBase64 = (image) => {
    return new Promise((resolve, reject) => {
      imageToBase64(URL.createObjectURL(image))
        .then((res) => {
          resolve(res);
        })
        .catch((err) => reject(err));
    });
  };

  const handleUploadImage = async (event) => {
    event.preventDefault();
    setUploadedImage(event.target.files[0]);
  };

  const handleSave = async (e) => {
    e.preventDefault();
    setSaveLoader(true);
    var data = {
      business_id: seller.id,
      store_name: e.target.store_name.value,
      first_name: e.target.first_name.value,
      last_name: e.target.last_name.value,
      email: e.target.email.value,
      phone: e.target.phone.value,
      address: e.target.address.value,
    };
    if (uploadedImage) {
      data.store_image_mime = uploadedImage.type;
      data.store_image_base64 = await toBase64(uploadedImage);
    }
    axiosInstance
      .patch(
        "/admin/sellers/details",
        { ...data },
        {
          headers: {
            Authorization: "Bearer " + props.user.access_token,
            "content-type": "application/json",
          },
        }
      )
      .then((res) => {
        setSeller(res.data.data);
        setSaveLoader(false);
        setUpdatedAlert(true);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getSettlements = () => {
    history.push(`/sellers/details/${props.match.params.id}/settlements`);
  };
  return loading ? (
    <Loader />
  ) : (
    <Aux>
      <Container fluid className="p-5">
        {updatedAlert && (
          <>
            <Alert
              variant="success"
              style={{
                backgroundColor: "#162424",
                border: "none",
                color: "#aaa",
                marginTop: 20,
                boxShadow: "0px 0px 10px #111",
              }}
            >
              <FiCheck size="20" strokeWidth="4" /> Details Updated
            </Alert>
          </>
        )}
        <Container
          fluid
          className="secondary-bg p-3"
          style={{ boxShadow: "0px 0px 20px #111" }}
        >
          <Row>
            <Col sm={12} md={4}>
              {seller && seller.store_image && !uploadedImage && (
                <>
                  <img
                    src={`${process.env.REACT_APP_S3_PREFIX}${seller.store_image}`}
                    className="mw-100"
                  />
                </>
              )}
              {uploadedImage ? (
                <>
                  <img
                    src={URL.createObjectURL(uploadedImage)}
                    className="mw-100"
                  />
                </>
              ) : null}
              <Form.Group className="mt-2">
                <Form.Label
                  htmlFor="upload-image"
                  className="custom-upload-btn"
                >
                  Upload Image <FiUploadCloud size="20" />
                </Form.Label>
                {/* <Form.File id='thumbnail-image' hidden accept="image/*" onChange={(event) => handleThumbnailUpload(event)} /> */}
                <Form.Control
                  id="upload-image"
                  type="file"
                  hidden
                  accept="image/*"
                  onChange={(event) => handleUploadImage(event)}
                />
              </Form.Group>
            </Col>
            <Col sm={12} md={8}>
              <div className="p-3">
                <div className="mb-2 d-flex justify-content-start align-items-center">
                  <div className="fs-30 flex-grow-1 text-secondary">
                    Seller Details
                  </div>
                  {/* {seller && seller.is_open ? (
                    <>
                      <Button variant="success">Open</Button>
                    </>
                  ) : (
                    <>
                      <Button variant="danger">Closed</Button>
                    </>
                  )} */}
                  <Button
                    variant="danger"
                    size="lg"
                    style={{ marginLeft: 10 }}
                    onClick={() => getMenu()}
                  >
                    View Menu <FiArrowRight size="15" />{" "}
                  </Button>
                </div>
                {/* <div className="d-flex align-items-center justify-content-start">
                  <Button variant="danger" size="sm">
                    <GoLocation size="15" style={{ marginRight: 7 }} />
                    {seller && seller.region}{" "}
                  </Button>
                  <Button
                    variant="secondary"
                    size="sm"
                    style={{ marginLeft: 10 }}
                  >
                    {seller && seller.category}{" "}
                  </Button>
                </div> */}
                <Form className="mt-2" onSubmit={(e) => handleSave(e)}>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      Store Name
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        type="text"
                        name="store_name"
                        defaultValue={seller && seller.store_name}
                        className="mw-100"
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      First Name
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        type="text"
                        name="first_name"
                        defaultValue={seller && seller.first_name}
                        className="mw-100"
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      Last Name
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        type="text"
                        name="last_name"
                        defaultValue={seller && seller.last_name}
                        className="mw-100"
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      Phone
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        type="text"
                        name="phone"
                        defaultValue={seller && seller.phone}
                        className="mw-100"
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      Email
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        type="text"
                        name="email"
                        defaultValue={seller && seller.email}
                        className="mw-100"
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      Address
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        as="textarea"
                        name="address"
                        defaultValue={seller && seller.address}
                        className="mw-100"
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="form-group">
                    <Form.Label column sm={2}>
                      Joined
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        type="text"
                        defaultValue={
                          seller && moment(seller.created_at).format("LL")
                        }
                        className="mw-100"
                        disabled
                      />
                    </Col>
                  </Form.Group>
                  <div className="d-flex justify-content-end align-items-center">
                    <Button
                      disabled={saveLoader}
                      variant="primary"
                      className="float-right"
                      type="submit"
                    >
                      {!saveLoader ? (
                        <>
                          Save &nbsp; <FiCheck strokeWidth="4" size="15" />
                        </>
                      ) : (
                        <>
                          Saving &nbsp;
                          <Spinner
                            variant="light"
                            animation="border"
                            size="sm"
                          />
                        </>
                      )}
                    </Button>
                  </div>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>

        <Row>
          <Col md={6} sm={12}>
            <div
              className="secondary-bg mt-4 p-3"
              style={{ boxShadow: "0px 0px 5px #111" }}
            >
              <div className="d-flex justify-content-start align-items-center">
                <div className="fs-20 text-secondary flex-grow-1">
                  Campaigns
                </div>
                {campaigns && campaigns.length === 0 && (
                  <Button variant="primary" onClick={() => handleOpen()}>
                    <FiPlus strokeWidth="4" size="15" /> &nbsp; Create Campaign
                  </Button>
                )}
              </div>
              <div className="text-secondary fs-30">
                {campaigns.length === 0 && <>No campaigns yet</>}
                {campaigns.length > 0 && (
                  <Row>
                    {campaigns.map((item, index) => {
                      return (
                        <Col sm="12" key={`campaign-${item.id}`}>
                          {item.image ? (
                            <div className="mw-100">
                              <img
                                src={`${process.env.REACT_APP_S3_PREFIX}${item.image}`}
                                className="mw-100"
                                style={{ height: 250, width: "100%" }}
                              />
                            </div>
                          ) : (
                            <div></div>
                          )}
                          <div
                            className="d-flex flex-column align-items-start justify-content-start campaign-box"
                            key={`campaign-${index}`}
                          >
                            <div className="flex-grow">{item.name}</div>
                            <span className="fs-40">{item.discount}%</span>
                            <Button
                              disabled={btnLoading}
                              variant="primary"
                              onClick={() => endCampaign(item.id)}
                              style={{ marginTop: 20 }}
                            >
                              {btnLoading ? (
                                <>
                                  Ending Campaign ...{" "}
                                  <Spinner animation="border" variant="light" />
                                </>
                              ) : (
                                <>End Campaign</>
                              )}
                            </Button>
                          </div>
                        </Col>
                      );
                    })}
                  </Row>
                )}
              </div>
            </div>
          </Col>
          <Col md={6} sm={12}>
            <div
              className="secondary-bg mt-4 p-3"
              style={{ boxShadow: "0px 0px 5px #111" }}
            >
              <div className="fs-20 text-secondary flex-grow-1">Accounts</div>
              <Row>
                <Col>
                  <div className="text-secondary fs-30">Total Revenue</div>
                  <span style={{ fontSize: "3rem", color: "#aaa" }}>
                    {" "}
                    <span className="fs-25" style={{ color: "#666" }}>
                      &#8377;
                    </span>
                    {(Math.round(seller.total_revenue * 100) / 100).toFixed(2)}
                  </span>
                </Col>
                <Col>
                  <div className="text-secondary fs-30">Current Balance</div>
                  <span style={{ fontSize: "3rem", color: "#aaa" }}>
                    {" "}
                    <span className="fs-25" style={{ color: "#666" }}>
                      &#8377;
                    </span>
                    {(Math.round(seller.account.balance * 100) / 100).toFixed(2)}
                  </span>
                </Col>
              </Row>

              <div className="mt-3 d-flex justify-content-start align-items-center">
                <div className="mt-2 text-secondary fs-30 flex-grow-1">
                  Last Settlement
                </div>
                <Button variant="primary" onClick={() => getSettlements()}>
                  View settlements <FiArrowRight size="15" />
                </Button>
              </div>
              {seller.latest_settlement ? (
                <div className='campaign-box text-secondary'>
                  <div>Amount : &#8377;{seller.latest_settlement.amount}</div>
                  {moment(seller.latest_settlement.created_at).format('Do MMM, YY  hh:mm A')}
                </div>
              ) : (
                <div className="campaign-box light-text">No settlement yet</div>
              )}
            </div>
          </Col>
        </Row>

        <TodaySpecial props={props} />
      </Container>

      <Modal
        show={campaignModal}
        onHide={handleClose}
        backdrop="static"
        className="mt-5 edit-item-modal"
        onSubmit={(e) => handleCreateCampaign(e)}
      >
        <Modal.Body>
          <div className="fs-20">Create a campaign</div>
          {campaignImage ? (
            <>
              <img
                src={URL.createObjectURL(campaignImage)}
                style={{ height: 200, width: "100%" }}
              />
            </>
          ) : (
            <div
              style={{
                height: 200,
                width: "100%",
                border: "3px dashed #666",
                borderRadius: 20,
              }}
              className="flex-column d-flex align-items-center justify-content-center"
            >
              No Image
            </div>
          )}
          <Form>
            <Form.Group className="mt-2">
              <Form.Label
                htmlFor="upload-campaign-image"
                className="custom-upload-btn"
              >
                Upload Image <FiUploadCloud size="20" />
              </Form.Label>
              <Form.Control
                id="upload-campaign-image"
                type="file"
                hidden
                accept="image/*"
                onChange={(event) => handleUploadCampaignImage(event)}
              />
            </Form.Group>
            <Form.Group as={Row} className="form-group mt-4">
              <Col sm={8}>
                {/* {invalidName && <span className='text-danger'>Required field</span>} */}
                <FloatingLabel label="Campaign Name">
                  <Form.Control
                    type="text"
                    name="name"
                    className="mw-100"
                    required
                  />
                </FloatingLabel>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="form-group mt-4">
              <Col sm={8}>
                {/* {invalidName && <span className='text-danger'>Required field</span>} */}
                <FloatingLabel label="Discount">
                  <Form.Control
                    type="number"
                    name="discount"
                    className="mw-100"
                    required
                  />
                </FloatingLabel>
              </Col>
            </Form.Group>
            <div className="mt-5 d-flex justify-content-end align-items-center">
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button
                disabled={btnLoading}
                variant="success"
                type="submit"
                style={{ marginLeft: "20px" }}
              >
                {btnLoading ? (
                  <>
                    Saving Changes ...{" "}
                    <Spinner animation="border" variant="light" />
                  </>
                ) : (
                  <>
                    Save Changes <FiCheck size="15" style={{ marginLeft: 7 }} />
                  </>
                )}
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </Aux>
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SellerDetails)
);
