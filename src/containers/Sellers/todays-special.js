import React, { useState, useEffect } from 'react';
import { withRouter, useHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Row, Col, Form, Button, Badge, Spinner, Modal, FloatingLabel } from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import axiosInstance from '../../utils/axiosAPI';
import { GoLocation } from 'react-icons/go';
import './index.scss';
import moment from 'moment';
import { FiTrash2, FiCheck, FiUploadCloud, FiPlus } from 'react-icons/fi';
import imageToBase64 from 'image-to-base64/browser';
import Switch from "react-switch";

const TodaySpecial = (props) => {

    const [loading, setLoading] = useState(true);
    const [btnLoading, setBtnLoading] = useState(false);
    const [items, setItems] = useState();
    const [menuSection, setMenuSection] = useState();
    const [editModal, setEditModal] = useState(false);
    const [editItem, setEditItem] = useState();
    const [uploadedImageSpl, setUploadedImageSpl] = useState();
    const [invalidName, setInvalidName] = useState(false);
    const [invalidPrice, setInvalidPrice] = useState(false);

    const handleClose = () => {
        setEditModal(false);
        setEditItem(null);
        setUploadedImageSpl(null);
    }
    const handleOpen = (item) => {
        setEditItem(item);
        setEditModal(true);
    }

    const addNewItem = () => {
        var newItem = {
            new_item: true,
            business_id: props.match.params.id,
            image: null,
            name: "",
            price: 0,
            is_veg: true,
            section: null
        }
        setEditItem(newItem);
        setEditModal(true);
    }

    const handleDelete = () => {
        axiosInstance.delete(`/admin/sellers/menu/delete/${editItem.id}`, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
            .then((res) => {
                console.log(res.data);
                var temp = [...items];
                var newTemp = temp.filter(item => item.id != editItem.id);
                setItems([...newTemp]);
                handleClose();
            })
            .catch((err) => console.log(err));
    }

    const handleUploadImageSpl = (event) => {
        event.preventDefault();
        console.log('image-updated');
        setUploadedImageSpl(event.target.files[0]);
    }
    const handleEditChange = (e, type) => {
        var temp = { ...editItem };
        if (type === 'section') {
            var section = e.target.value;
            if (section === 'No Section') temp.section = null;
            else temp.section = section;
        }
        if (type === 'name') {
            temp.name = e.target.value;
        }
        if (type === 'price') {
            temp.price = parseInt(e.target.value);
        }
        if (type === 'is_veg') {
            temp.is_veg = e;
        }
        setEditItem({ ...temp });
    }

    const toBase64 = (image) => {
        return new Promise((resolve, reject) => {
            imageToBase64(URL.createObjectURL(image))
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => reject(err));
        })
    }
    const handleSubmit = async () => {
        let data = editItem;
        if (data.name === "") {
            setInvalidName(true);
            return false;
        }
        if (data.price <= 0) {
            setInvalidPrice(true);
            return false;
        }
        setBtnLoading(true);
        if (uploadedImageSpl) {
            data.image_mime = uploadedImageSpl.type;
            data.image_base64 = await toBase64(uploadedImageSpl);
        }
        if (data.section === 'no-section') data.section = null;
        console.log(data);
        if (data.new_item) {
            axiosInstance.post('/admin/todays-spl', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...items];
                    temp.push(res.data.data)
                    setItems([...temp]);
                    setInvalidName(false);
                    setInvalidPrice(false);
                    setUploadedImageSpl(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => {
                    console.log(err)
                }
                );
        }
        else {
            axiosInstance.post('/admin/sellers/menu/update', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...items];
                    var index = temp.findIndex(item => item.id === res.data.data.id);
                    temp[index] = res.data.data;
                    setItems([...temp]);
                    setUploadedImageSpl(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => console.log(err));
        }
    }

    const fetchMenuSections = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/menu-section', { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    setMenuSection(res.data.data);
                    resolve();
                })
                .catch((err) => {
                    console.log(err);
                    reject();
                })
        })
    }

    const fetchTodaysSpecials = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/todays-spl/' + props.match.params.id, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then(res => {
                    console.log(res.data.data);
                    setItems(res.data.data);
                    resolve();
                })
                .catch(err => {
                    console.log(err);
                    reject();
                })
        })
    }

    useEffect(() => {
        (async () => {
            try {
                await fetchTodaysSpecials();
                await fetchMenuSections();
            }
            catch (e) {
                console.log(e);
            }
            finally {
                setLoading(false);
            }
        })();
    }, [])
    return loading ? <Loader /> : (
        <>
            <div className='secondary-bg mt-5 p-3' style={{ boxShadow: '0px 0px 5px #111' }}>
                <div className='d-flex justify-content-start align-items-center'>
                    <div className='fs-20 text-secondary flex-grow-1'>Today's Specials</div>
                    <Button variant='primary' onClick={() => addNewItem()}><FiPlus strokeWidth='4' size='15' /> &nbsp; Add Today's Special</Button>
                </div>
                {items &&
                    <Row>
                        {items.map((item, index) => {
                            return (
                                <Col lg={3} md={6} key={`menu-${index}`} >
                                    <div className='mt-4 menu-item-container' onClick={() => handleOpen(item)}>
                                        {item.image
                                            ? <div className='mw-100'><img src={`${process.env.REACT_APP_S3_PREFIX}${item.image}`} className='mw-100' style={{ height: 250, width: '100%' }} /></div>
                                            : <div style={{ height: 250 }}></div>
                                        }
                                        <div className='text-light fs-20 mt-3'>{item.name}</div>
                                        <div><span className='text-light fs-15'>&#8377;</span><span className='text-light fs-20'>{item.price}</span></div>
                                        <Badge bg={item.is_veg ? 'success' : 'danger'}> {item.is_veg ? 'Veg' : 'Non-Veg'} </Badge>
                                        <Badge bg='light' style={{ color: '#111', marginLeft: 8, fontSize: 10 }}>{item.section ? item.section : ''}</Badge>

                                    </div>
                                </Col>
                            );
                        })}
                    </Row>
                }
                <div className='text-secondary fs-30'>
                    {items.length === 0 && <>No special items today </>}
                </div>
            </div>
            {editItem &&
                <Modal show={editModal} onHide={handleClose} backdrop="static" className='edit-item-modal'>
                    <Modal.Body>
                        <div className='fs-20'>Edit Item</div>
                        {/* ITEM IMAGE */}
                        <Row>
                            <Col sm={4}>
                                <div className='mt-5'>
                                    {editItem.image && !uploadedImageSpl
                                        ? <img src={`${process.env.REACT_APP_S3_PREFIX}${editItem.image}`} style={{ height: 200, width: 200 }} />
                                        :
                                        uploadedImageSpl
                                            ? <><img src={URL.createObjectURL(uploadedImageSpl)} style={{ height: 200, width: 200 }} /></>
                                            : <div style={{ height: 200, width: 200, border: '3px dashed #666', borderRadius: 20 }} className='flex-column d-flex align-items-center justify-content-center'>
                                                No Image
                                            </div>
                                    }
                                    <Form.Group className='mt-2'>
                                        <Form.Label htmlFor='upload-image-2' className='custom-upload-btn'>Upload Image <FiUploadCloud size='20' /></Form.Label>
                                        {/* <Form.File id='thumbnail-image' hidden accept="image/*" onChange={(event) => handleThumbnailUpload(event)} /> */}
                                        <Form.Control id='upload-image-2' type='file' hidden accept="image/*" onChange={(event) => handleUploadImageSpl(event)} />
                                    </Form.Group>
                                </div>
                            </Col>
                            <Col sm={8}>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    <Col sm={8}>
                                        <FloatingLabel controlId="floatingSelect" label="Category">
                                            <Form.Select aria-label="Floating label select example" name='section' defaultValue={editItem.section} onChange={(e) => handleEditChange(e, 'section')}>
                                                <option disabled>Open this select menu</option>
                                                <option value='no-section'>No Section</option>
                                                {menuSection && menuSection.map((item, index) => {
                                                    return (
                                                        <option key={`option-${index}`} value={item.section_name}>{item.section_name}</option>
                                                    );
                                                })}
                                            </Form.Select>
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    {/* <Form.Label column sm={2}>Store Name</Form.Label> */}
                                    <Col sm={8}>
                                        {invalidName && <span className='text-danger'>Required field</span>}
                                        <FloatingLabel label='Item Name'>
                                            <Form.Control type="text" name='name' defaultValue={editItem && editItem.name} className='mw-100' required onChange={(e) => handleEditChange(e, 'name')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    {/* <Form.Label column sm={2}>Store Name</Form.Label> */}
                                    <Col sm={8}>
                                        {invalidPrice && <span className='text-danger'>Required field</span>}
                                        <FloatingLabel label='Price'>
                                            <Form.Control type="text" name='price' defaultValue={editItem && editItem.price} className='mw-100' required onChange={(e) => handleEditChange(e, 'price')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>

                                <Col sm={8}>
                                    <div className='p-2 mt-2 d-flex align-items-start justify-content-start'>
                                        <div className='flex-grow-1'>
                                            Vegetarian
                                        </div>
                                        <Switch
                                            onChange={(e) => handleEditChange(e, 'is_veg')}
                                            checked={editItem.is_veg}
                                            offColor='#fa4b4b'
                                            height={20}
                                            width={40}
                                        />
                                    </div>
                                </Col>

                                <div className='mt-5 d-flex justify-content-end align-items-center' >
                                    <Button variant="secondary" onClick={handleClose}>
                                        Cancel
                                    </Button>
                                    <Button variant="primary" onClick={() => handleDelete()} style={{ marginLeft: '20px' }}>
                                        Delete this item <FiTrash2 size='15' style={{ marginLeft: 7 }} />
                                    </Button>
                                    <Button disabled={btnLoading} variant="success" onClick={() => handleSubmit()} style={{ marginLeft: '20px' }}>
                                        {btnLoading
                                            ? <>Saving Changes ... <Spinner animation='border' variant='light' /></>
                                            : <>Save Changes <FiCheck size='15' style={{ marginLeft: 7 }} /></>
                                        }

                                    </Button>
                                </div>
                            </Col>
                        </Row>

                    </Modal.Body>
                </Modal>
            }
        </>
    );
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TodaySpecial));
