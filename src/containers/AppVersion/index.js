import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Table, Row, Col, Button, Modal, Form, Spinner } from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import BreadCrumb from '../../components/BreadCrumb';
import DeleteConfirm from '../../components/DeleteConfim';
import axiosInstance from '../../utils/axiosAPI';
import { FiPlus, FiChevronLeft, FiChevronRight, FiCheck, FiX } from 'react-icons/fi';
import { Switch } from "pretty-checkbox-react";
import ReactPaginate from 'react-paginate';
import * as actionTypes from '../../store/actions';
import moment from "moment";
import "./index.scss";


function AppVersion(props) {
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [appVersionList, setAppVersionList] = useState();
  const [breadCrumbs, setBreadCrumbs] = useState();

  const [addVersionModal, setAddVersionModal] = useState(false);
  const [saveLoading, setSaveLoading] = useState(false);

  const showAddVersionModal = () => { setAddVersionModal(true) };
  const hideAddVersionModal = () => { setAddVersionModal(false) };

  const handleAddVersion = (e) =>{
    e.preventDefault();
    setSaveLoading(true);
    let data = {
      "platform" : e.target.platform.value,
      "version" : e.target.version.value,
      "is_mandatory" : e.target.is_mandatory.checked
    }
    axiosInstance.post('/app-version/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
    .then(res => {
      window.location.reload();
    })
    .catch(err => {
      console.log(err);
      setSaveLoading(false);
      hideAddVersionModal();
    })
  }

  const handlePageClick = (data) => {
    const pageRequested = data.selected + 1;
    fetchPaginatedData(pageRequested);
  }
  const fetchPaginatedData = (page) => {
    axiosInstance.get('/api-version/', { headers: { Authorization: 'Bearer ' + props.user.access_token }, params: { page: page } })
      .then(res => {
        setAppVersionList(res.data);
      })
      .catch(err => {
        console.log(err);
      })
  }
  const fetchAppVersions = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/app-version/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          setAppVersionList(res.data);
          resolve()
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    setLoading(true);
    document.title = 'App Version | Diall Admin';
    setBreadCrumbs([
      { location: "Dashboard", path: '/dashboard' },
      { location: "App Version", path: '/app-version' }
    ]);
    (async () => {
      try {
        await fetchAppVersions();
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);
  return loading ? <div className='h-100 d-flex justiy-content-center align-items-center'> <Loader /></div> : (
    <Aux>
      <Container fluid className='mx-0 mt-5 mb-5'>
        <Row>
          <Col>
            <BreadCrumb list={breadCrumbs} />
          </Col>
          <Col>
            <Button variant='primary' className='float-right' onClick={() => showAddVersionModal()}><FiPlus strokeWidth="4" className="mr-2" />Add version</Button>
          </Col>
        </Row>
        <div className='mt-2'>
          {appVersionList &&
            <>
              <div className='table-responsive-md'>
                <Table size='sm'>
                  <thead>
                    <tr>
                      <th>Platform</th>
                      <th>App Version</th>
                      <th>Mandatory</th>
                      <th>Provisioned at</th>
                    </tr>
                  </thead>
                  <tbody>
                    {appVersionList.results.map((item, index) =>
                      <tr key={item.id}>
                        <td>{item.platform} </td>
                        <td>{item.version}</td>
                        <td>{item.is_mandatory ? <FiCheck color="green" strokeWidth="4" /> : <FiX color="red" strokeWidth="4" />}</td>
                        <td>{moment(item.provisioned_at).format("MMMM Do, YYYY")}</td>
                      </tr>
                    )
                    }
                  </tbody>
                </Table>
              </div>

              {appVersionList.count > 20 &&
                <ReactPaginate
                  previousLabel={<FiChevronLeft size={15} />}
                  nextLabel={<FiChevronRight size={15} />}
                  breakLabel={'...'}
                  breakClassName={'break-me'}
                  pageCount={Math.ceil(appVersionList.count / 20)}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={0}
                  onPageChange={(event) => handlePageClick(event)}
                  containerClassName={'pagination'}
                  subContainerClassName={'pages pagination'}
                  activeClassName={'active'}
                />
              }
            </>
          }
        </div>
      </Container>
      {/* <DeleteConfirm show={deleteConfirm} hide={hideDeleteConfirm} confirm={handleDelete} /> */}

      <Modal className='add-version-modal' show={addVersionModal} onHide={hideAddVersionModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add new app release</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e) => handleAddVersion(e)}>
            <Form.Group as={Row} className='mt-3'>
              <Form.Label column sm="3">Platform</Form.Label>
              <Col sm="7">
                <Form.Control as="select" name="platform" required>
                  <option value="ios">iOS</option>
                </Form.Control>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className='mt-3'>
              <Form.Label column sm="3">New version</Form.Label>
              <Col sm="7">
                <Form.Control type="number" name="version" step=".1" required />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className='mt-3'>
              <Col sm="7">
                <Form.Check type="checkbox"  name="is_mandatory" label="is mandatory"/>
              </Col>
            </Form.Group>
            <div className='d-flex justify-content-end align-items-center'>
              <Button disabled={saveLoading} variant="primary" type="submit">
                {saveLoading 
                  ? <><Spinner animation="border" size="sm" /> Saving </>
                  : <>Save</>
                }
              </Button>
              <Button className='ml-2' variant="light" onClick={hideAddVersionModal}>
                Close
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppVersion));