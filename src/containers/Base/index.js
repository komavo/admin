import React from "react";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";

function Base(props) {
  return props.isAuthenticated
    ? (< Redirect to = "/dashboard" />)
    : (< Redirect to = "/login" />);
}

const mapStateToProps = state => {
  return {
    user: state.user, 
    isAuthenticated: state.isAuthenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Base));
