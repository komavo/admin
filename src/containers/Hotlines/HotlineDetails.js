import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Aux from "../../hoc/_Aux";
import BreadCrumb from "../../components/BreadCrumb";
import Loader from '../../components/Loader';
import ContactDisplay from '../../components/ContactDisplay';
import HotlineModal from '../../components/HotlineModal';
import axiosInstance from '../../utils/axiosAPI';
import { Container, Form, Card, Row, Col, Button, Spinner } from "react-bootstrap";
import { FiEdit2, FiPlus } from 'react-icons/fi';
import moment from 'moment';


const days = {
  "1": "Monday",
  "2": "Tuesday",
  "3": "Wednesday",
  "4": "Thursday",
  "5": "Friday",
  "6": "Saturday",
  "7": "Sunday"
}

const HotlineDetails = (props) => {
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [category, setCategory] = useState();
  const [contacts, setContacts] = useState();
  const [loading, setLoading] = useState(true);
  const [hotline247, setHotline247] = useState();
  const [saveLoader, setSaveLoader] = useState();
  const [editHotlineData, setEditHotlineData] = useState();

  const [addHotlineModal, setAddHotlineModal] = useState(false);
  const showHotlineAdd = () => { setAddHotlineModal(true) };
  const hideHotlineAdd = () => { setEditHotlineData(null); setAddHotlineModal(false) };

  const updateCategoryTimeToUTC = (data) =>{
    for(let contact of data.contacts){
      contact.active_from_time = moment(contact.active_from_time, "hh:mm:ss").add(5, 'hours').format("HH:mm:ss");
      contact.active_to_time = moment(contact.active_to_time, "hh:mm:ss").add(5, 'hours').format("HH:mm:ss");
    }
    setCategory(data);
    setContacts(data.contacts)
  }
  const getCategoryDetails = (id) => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/hotlines/' + id + '/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          updateCategoryTimeToUTC(res.data);
          setHotline247(res.data.is_hotline247);
          setBreadCrumbs([
            { location: "Dashboard", path: "/dashboard" },
            { location: "Hotlines", path: "/hotlines" },
            { location: res.data.category_name, path: '#' },
          ]);
          resolve();
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    document.title = "Hotlines | Diall Admin";
    (async () => {
      try {
        await getCategoryDetails(props.match.params.id);
      }
      catch (e) {
        console.log('ok', e);
      }
      finally {
        setLoading(false);
      }
    })();
  }, []);

  const handleHotline247 = (e) => {
    setHotline247(e.target.checked);
  }

  const handleFormSubmit = (e) => {
    e.preventDefault();
    setSaveLoader(true);
    let data = {
      category_id: props.match.params.id,
      is_hotline247: hotline247,
      contact_number: e.target.contact_number.value,
      contact_name: e.target.contact_name.value
    }
    axiosInstance.patch('/hotlines/' + props.match.params.id + '/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        window.location.reload();
      })
  }
  const handleModalData = (data) => {
    // console.log(data);
    // let cleaned_data = {
    //   category_id : props.match.params.id,
    //   is_hotline247 : false,
    //   contacts : [...data]
    // }

    axiosInstance.patch('/hotlines/' + props.match.params.id + '/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        // console.log(res.data);
        window.location.reload();
      })
  }
  const handleDeleteContact = (data) =>{
    axiosInstance.post('/hotlines/delete-contact/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        // console.log(res.data);
        window.location.reload();
      })
  }
  const handleDeleteTiming = (id) =>{
    axiosInstance.delete('/hotlines/' + id + '/delete-timing/' , { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        // console.log(res.data);
        window.location.reload();
      })
  }
  
  const handleEditContact = (data) =>{
    let temp = category;
    temp.contacts = temp.contacts.filter(contact => contact.contact_number === data.contact_number);
    let editData = {
      category: temp,
      contact_number : data.contact_number
    } 
    setEditHotlineData({...editData});
    // setAddHotlineModal(true);
    showHotlineAdd();
  }
  return loading ? <Loader /> : (
    <Aux>
      <Container className="mb-5 pb-5">
        <BreadCrumb list={breadCrumbs} />
        <Card className="px-4 mt-3">
          <span className='text-secondary fs-13 pt-3'>* Time entered here is in Eastern Time (US/Eastern)</span>
          <Row>
            <Col sm='12' md='8'>
            <Form className="mt-2" onSubmit={(event) => handleFormSubmit(event)}>
            <Form.Group as={Row} controlId="formPlaintextEmail">
              <Form.Label column sm="2">
                Category Name:
              </Form.Label>
              <Col sm="10">
                <div className='fs-13 text-secondary pt-2'>{category && category.category_name}</div>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="formPlaintextPassword">
              <Form.Label column sm="2">
                Order
              </Form.Label>
              <Col sm="10">
                <div className='fs-13 text-secondary pt-2'>{category && category.order}</div>
              </Col>
            </Form.Group>
            </Form>
            </Col>
            <Col sm='12' md='4'>
              <div className='d-flex justify-content-center align-items-center flex-column'>
                <span className='text-secondary fs-13 strong'>HITS</span>
                <span className='fs-40 text-secondary'>{category.category_hits}</span>
              </div>
            </Col>
          </Row>
        </Card>

        { !hotline247 && <h6 className="fs-16 mt-3 mx-2 p-1 text-muted">Hotlines</h6>}
          
        {contacts && contacts.length > 0 && 
            <ContactDisplay 
              category={category} 
              days={days}
              deleteContact={handleDeleteContact}
              deleteTiming={handleDeleteTiming}
              editContact={handleEditContact}
              user={props.user}
            />
         }
        {
          contacts && !hotline247 &&
          <div className='p-2 mt-3'>
            <Button variant='success' size='sm' onClick={() => {setEditHotlineData(null); showHotlineAdd()}}><FiPlus strokeWidth='4'/> Add Hotline</Button>
          </div>
        }
        <HotlineModal show={addHotlineModal} hide={hideHotlineAdd} passData={(data) => handleModalData(data)} editData={editHotlineData}/>
      </Container>
    </Aux>
  );
};


const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HotlineDetails));