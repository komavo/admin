import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Aux from "../../hoc/_Aux";
import BreadCrumb from "../../components/BreadCrumb";
import { Container, Form, Button, Table, Modal } from "react-bootstrap";
import axiosInstance from '../../utils/axiosAPI';
import Loader from '../../components/Loader';
import * as actionTypes from '../../store/actions';

const Hotlines = (props) => {
  const history = useHistory();
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [categories, setCategories] = useState();
  const [loading, setLoading] = useState(true);

  const getDetails = (event) =>{
    history.push(`/hotlines/details/${event.target.id}`);
  }
  const getCategories = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/hotlines/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          setCategories(res.data);
          resolve();
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    document.title = "Hotlines | Diall Admin";
    setBreadCrumbs([
      { location: "Dashboard", path: "/dashboard" },
      { location: "Hotlines", path: "/hotlines" },
    ]);
    (async () => {
      try {
        await getCategories();
      }
      catch (e) {
        console.log('ok',e);
      }
      finally {
        setLoading(false);
      }
    })();
  }, []);

  return loading ? <Loader /> : (
    <Aux>
      <Container fluid className="mx-0 mt-5">
        <BreadCrumb list={breadCrumbs} />

        <div className="table-responsive-md mt-2">
          <Table size="sm">
            <thead>
              <tr>
                <th>CATEGORY NAME</th>
                <th>ORDER</th>
              </tr>
            </thead>
            <tbody>
              { categories && categories.map((cat, index) => <tr key={index}>
                  <td className='link'><a onClick={getDetails} id={cat.category_id}>{cat.category_name}</a></td>
                  <td>{cat.order}</td>
              </tr>
              )}
            </tbody>
          </Table>
        </div>
      </Container>
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateNotifications: (payload) => dispatch({ type: actionTypes.UPDATE_POST_NOTIFICATIONS, payload: payload })
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Hotlines));