import React, { useState } from 'react';
import Aux from '../../hoc/_Aux';
import {FiArrowRight} from "react-icons/fi"
import { Container, Form, Button, Spinner } from 'react-bootstrap';
import { withRouter, useHistory, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './index.scss';
import FullLogo from '../../assets/images/full-logo.svg';
import axiosInstance from '../../utils/axiosAPI';
import * as actionTypes from '../../store/actions';

function Login(props) {
  let history = useHistory(); 

  const [credError, setCredError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState('default');

  const handleLogin = (event) => {
    event.preventDefault();
    setLoading(true);
    if(event.target.username.value === "" || event.target.password.value === ""){
      return false;
    }
    const data = {
      username : event.target.username.value,
      password : event.target.password.value
    }
    axiosInstance.post('/admin/login', {...data})
    .then(res => {
      console.log(res.data);
      if(res.data.status_code === 200){
        let payload = {
          username: res.data.data.username,
          access_token: res.data.data.access_token, 
        }
        props.updateTokens({...payload});
        history.push('/dashboard');
        setLoading(false);
      }
      else{
        setCredError(true);
        setLoading(false);
      }
    })
  }

  const handlePasswordReset = (e) =>{
    e.preventDefault();
    setLoading(true);
    axiosInstance.post('reset-password/', {email: e.target.email.value})
    .then(res => {
      console.log(res.data);
      setStatus('link-sent');
      setLoading(false);
    })
    .catch(err => {
      console.log('There was an error sending reset link');
    })
  }
  return props.isAuthenticated ? <Redirect to='/dashboard' /> : (
    <Aux>
        <Container fluid className='h-100 d-flex justify-content-center align-items-center flex-column custom-bg' >
            <div className='d-flex justify-content-center align-items-center'>
            <img src={FullLogo} style={{width: 200}}/>
            </div>
            { status === 'default' && 
              <div className='login-box p-3 mt-2 d-flex justify-content-center align-items-center'>
                  <Form className='d-flex justify-content-center align-items-center flex-column p-3' onSubmit={(event) => handleLogin(event)} autoComplete='off'>
                    { credError && <span className='text-danger fs-11 m-2'>Wrong Credentials</span>}
                    <Form.Control type="text" name='username' placeholder="Username" />
                    <Form.Control  className='mt-3' type="password" name='password' placeholder="Password" />
                    
                    <div className='mt-2 d-flex justify-content-end align-items-center w-100'>
                      <a href='#' className='fs-12' onClick={() => setStatus('forgot-password')}>Forgot password ?</a>
                    </div>

                    <Button  className='mt-3' variant='primary' type='submit'>
                      { loading 
                      ? <>Loggin in... <Spinner className='ml-2' animation='border' size='sm' /> </>
                      : <>Login <FiArrowRight /> </>
                      }
                    </Button>
                  </Form>
              </div>
            }
            { status === 'forgot-password' && 
              <div className='login-box bg-light p-3 mt-2 d-flex flex-column justify-content-center align-items-center'>
                Please enter your email Id used for logging in.
                <Form className='d-flex justify-content-center align-items-center flex-column p-3' onSubmit={(event) => handlePasswordReset(event)} autoComplete='off'>
                  <Form.Control type="text" name='username' placeholder="Username" />
                  <Button  className='mt-3' variant='primary' type='submit'>
                    { loading 
                    ? <>Sending link... <Spinner className='ml-2' animation='border' size='sm' /> </>
                    : <>Send reset link <FiArrowRight /> </>
                    }
                  </Button>
                </Form>
            </div>
            }
            { status === 'link-sent' && 
            <div className='login-box bg-light p-3 mt-2 d-flex flex-column justify-content-center align-items-center' style={{maxWidth: "500px"}}>
              We have received your request to reset your password. If there is an account associated with the email provided, 
              a password reset link will be sent. Don't forget check your spam folder incase you cannot find it in primary folder 
          </div>
          }  
        </Container>
      </Aux>
    );
  };
  
  const mapStateToProps = state => {
    return {
      isAuthenticated: state.isAuthenticated,
      user: state.user
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      updateTokens: (payload) => dispatch({ type: actionTypes.UPDATE_TOKENS, payload: payload })
    }
  };
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));