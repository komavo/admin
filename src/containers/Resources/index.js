import React, { useState, useEffect } from 'react';
import { withRouter, useHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container } from 'react-bootstrap';
import { FiTrash, FiPlus } from 'react-icons/fi';

import Aux from '../../hoc/_Aux';
import BreadCrumb from '../../components/BreadCrumb';
import DeleteConfirm from '../../components/DeleteConfim';
import DataTable from "../../components/DataTable";

import { apiCall } from "../../utils/services";

const Resources = () => {
  const history = useHistory();

  const [total, setTotal] = useState(0);
  const [refresh, setRefresh] = useState(0);
  const [deleting, setDeleting] = useState(false);
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [deleteConfirm, setDeleteConfirm] = useState(false);

  const handleDelete = async () => {
    setDeleting(true);
    await apiCall(`/resources/${deleteConfirm.resource_id}/`, {
      method: 'delete'
    });
    setDeleting(false);
    setDeleteConfirm(false);
    setRefresh(refresh + 1);
  }

  useEffect(() => {
    document.title = 'Resources | Diall Admin';
    setBreadCrumbs([
      { location: "Dashboard", path: '/dashboard' },
      { location: "Resources", path: '/resources' }
    ]);
  }, []);

  return (
    <Aux>
      <Container fluid={true} className='my-2'>
        <BreadCrumb list={breadCrumbs} />
        <div className="d-flex align-items-center justify-content-start px-0 py-2">
          <span>Total Resources: {total}</span>
          <span className="ml-auto">
            <Link to='/resources/add' className="btn btn-success fs-12 d-flex align-items-center"><FiPlus strokeWidth='4' className='mr-2' />Create Resource</Link>
          </span>
        </div>

        <div className='mt-2'>
          <DataTable
            size="lg"
            columns={[
              {
                text: "Title",
                key: "resource_title",
                type: "text",
                sortable: true,
                style: { maxWidth: 200 }
              },
              {
                text: "Author",
                key: "resource_author",
                type: "text",
                sortable: true,
                style: { maxWidth: 100 }
              },
              {
                text: "Category",
                key: "category[0].category_name",
                type: "text",
                style: { maxWidth: 100 }
              },
              {
                text: "Media",
                key: "resource_media[0].resource_media_type",
                type: "text",
                style: "text-capitalize",
                style: { width: 75 }
              },
              {
                text: "Date Created",
                key: "created_at",
                align: 'text-right',
                type: "datetime",
                sortable: true
              }
            ]}
            actions={[
              {
                icon: <FiTrash size="14" strokeWidth="2" />,
                tooltip: "Delete",
                onClick: (item) => setDeleteConfirm(item),
                class: 'btn-danger'
              }
            ]}
            searchable={true}
            filters={[
              {
                name: 'filter_resource_type',
                type: 'select',
                options: [{ value: "ad", text: "Ad" }, { value: "post", text: "Post" }],
                placeholder: "Filter by Post Type"
              },
              {
                name: 'filter_resource_media',
                type: 'select',
                options: [{ value: "image", text: "Image" }, { value: "video", text: "Video" }, { value: "none", text: "No Media" }],
                placeholder: "Filter by Media"
              },
              {
                name: 'filter_category',
                type: 'select',
                options: [{ value: 2, text: "Mental Health" }, { value: 3, text: "Suicide Prevention" }, { value: 4, text: "Substance Abuse" }, { value: 5, text: "BIPOC Focused Aid" }, { value: 6, text: "Home Loss & Shelter" }, { value: 7, text: "Eating Disorder" }, { value: 8, text: "Domestic Violence" }, { value: 9, text: "Sexual Misconduct" }, { value: 10, text: "LGBQT+ Focused Aid" }],
                placeholder: "Filter by Category"
              },
              {
                name: 'filter_created_at',
                type: 'daterange',
                placeholder: "Filter by Date Created"
              }
            ]}
            onRowClick={(value) => history.push(`/resources/details/${value.resource_id}`)}
            setTotal={(total) => setTotal(total)}
            baseUrl="resources/"
            refresh={refresh}
          />
        </div>
      </Container>
      <DeleteConfirm
        show={deleteConfirm}
        hide={() => setDeleteConfirm(false)}
        confirm={handleDelete}
        deleting={deleting}
      />
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Resources));