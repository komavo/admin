import React, { useState, useEffect } from 'react';
import { withRouter, useHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Row, Col, Form, Button, Spinner, Badge } from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import BreadCrumb from '../../components/BreadCrumb';
import DeleteConfirm from '../../components/DeleteConfim';
import axiosInstance from '../../utils/axiosAPI';
import { FiX, FiPlus, FiXCircle } from 'react-icons/fi';
import './index.scss';
import AWS from 'aws-sdk';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

AWS.config.update({
    accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY
});
const s3 = new AWS.S3({
    params: { Bucket: process.env.REACT_APP_AWS_STORAGE_BUCKET_NAME },
    region: process.env.REACT_APP_AWS_S3_REGION_NAME
})

function ResourceDetails(props) {
    const history = useHistory();
    const [breadCrumbs, setBreadCrumbs] = useState();
    const [categories, setCategories] = useState();
    const [resourceContent, setResourceContent] = useState("");
    const [media, setMedia] = useState();
    const [mediaThumbnail, setMediaThumbnail] = useState();
    const [thumbnailRequired, setThumbnailRequired] = useState(false);
    const [selectedCategories, setSelectedCategories] = useState([]);
    const [emptyCategory, setEmptyCategory] = useState(false);
    const [loading, setLoading] = useState(true);
    const [saveLoader, setSaveLoader] = useState(false);
    const [fileUploadProgress, setFileUploadProgress] = useState(null);
    const [deleteMediaWhich, setDeleteMediaWhich] = useState(null);
    const [deleteConfirm, setDeleteConfirm] = useState(false);
    const showDeleteConfirm = (type) => { setDeleteMediaWhich(type); setDeleteConfirm(true); }
    const hideDeleteConfirm = () => { setDeleteMediaWhich(null); setDeleteConfirm(false); }



    const handleEditorChange = (e) => {
        setResourceContent(e);
    }


    const handleMediaDelete = () => {
        if (deleteMediaWhich === 'media') {
            setMedia(null);
            setMediaThumbnail(null);
            setThumbnailRequired(false);
        }
        else {
            setMediaThumbnail(null);
            setThumbnailRequired(true);
        }
        // setResourceMedia(resourceMedia.filter((item, index) => index !== deleteMediaID));
        hideDeleteConfirm();
    }

    const handleSelectCategory = (event) => {
        if (selectedCategories.filter(item => item.category_id === parseInt(event.target.value)).length > 0) {
            return
        }
        let category = categories.find(item => item.category_id === parseInt(event.target.value));
        setSelectedCategories([...selectedCategories, category]);
    }

    const handleDeselectCategory = (id) => {
        let categories = selectedCategories.filter(item => item.category_id !== id);
        setSelectedCategories(categories);
    }

    const handleFileUpload = (event) => {
        event.preventDefault();
        if (event.target.files[0].type.split('/')[0] === 'video') {
            setThumbnailRequired(true);
        }
        setMedia({
            type: event.target.files[0].type.split('/')[0],
            file: event.target.files[0],
            thumbnail: null
        });
    }
    const handleThumbnailUpload = (event) => {
        event.preventDefault();
        let temp = media;
        temp.thumbnail = event.target.files[0];
        setMedia(temp);;
        setMediaThumbnail(event.target.files[0]);
    }

    const fetchCategories = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/categories/', { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'multipart/form-data' } })
                .then(res => {
                    setCategories(res.data);
                    setBreadCrumbs([
                        { location: "Dashboard", path: '/dashboard' },
                        { location: "Resources", path: '/resources' },
                        { location: "Add New Resource", path: '/resources/add' }
                    ]);
                    resolve()
                })
                .catch(err => {
                    reject();
                })
        })
    }

    const sanitizeString = (s) => {
        return s.replace(/[^a-z0-9]/gi, '_').toLowerCase().replace(/_{2,}/g, '_')
    }

    useEffect(() => {
        document.title = 'Add Resource | Diall Admin';
        setLoading(true);
        (async () => {
            try {
                await fetchCategories();
            }
            catch (e) {
                console.log(e);
            }
            finally {
                setLoading(false);
            }
        })();
    }, []);
    
    const getUploadedFileKey = (filetype, resource_title) => {
        let result = {};

        //Media
        return new Promise((resolve, reject) => {
            let file_name = 'Resources/' + sanitizeString(resource_title) + '/' + Date.now().toString() + (filetype === 'media' ? '.' : '_thumbnail.') + media.file.name.split(".")[1]
            const params = {
                ACL: 'public-read',
                Key: file_name,
                ContentType: (filetype === 'media' ? media.file.type : mediaThumbnail.type),
                Body: (filetype === 'media' ? media.file : mediaThumbnail),
            }
            var putObjectPromise = s3.putObject(params).promise();
            putObjectPromise
                .then(function (data) {
                    resolve(file_name);
                }).catch(function (err) {
                    console.log(err);
                    reject(err);
                });
        });
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault();

        if(!saveLoader) {
            let formData = {
                resource_title: event.target.resource_title.value,
                resource_sub_title: event.target.resource_sub_title.value,
                resource_content: resourceContent,
                resource_type: event.target.resource_type.value,
                resource_author: event.target.resource_author.value,
                resource_author_url: event.target.resource_author_url.value,
                category: selectedCategories
            }
            setSaveLoader(true);
            
            if (selectedCategories.length === 0) {
                setEmptyCategory(true);
                setSaveLoader(false);
                return
            }

            if (!media) {
                axiosInstance.post('/resources/', formData, { headers: { 'Authorization': 'Bearer ' + props.user.access_token } })
                    .then(res => {
                        history.push('/resources');
                    })
                setSaveLoader(false);
                return
            }

            if (media.type === 'video' && !mediaThumbnail) {
                alert('Please upload a thumbnail for the video');
                setSaveLoader(false);
                return
            }
            if (resourceContent === "") {
                alert('Please write some content...');
                setSaveLoader(false);
                return
            }
            
            formData.media_type = media.type;
            formData.media_key =  await getUploadedFileKey("media", event.target.resource_title.value);
            if (media.type === 'video'){
                formData.thumbnail_key = await getUploadedFileKey("thumbnail", event.target.resource_title.value);
            }
            axiosInstance.post('/resources/', formData, { headers: { 'Authorization': 'Bearer ' + props.user.access_token } })
                .then(res => {
                    history.push('/resources');
               })
        }
    }
    return (loading ? <Loader />
        :
        <Aux>
            <Container className='px-0 mt-5 mb-5'>
                <BreadCrumb list={breadCrumbs} />
                <div className='bg-white p-4'>
                    <Form className='bg-white' onSubmit={(event) => { setSaveLoader(true); handleFormSubmit(event) }}>

                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Header*</Form.Label>
                            <Col sm={10}><Form.Control type="text" name='resource_title' className='mw-100' required /></Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Sub-header</Form.Label>
                            <Col sm={10}><Form.Control type="text" name='resource_sub_title' className='mw-100'/></Col>
                        </Form.Group>


                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Body*</Form.Label>
                            <Col sm={10}>
                            <ReactQuill 
                                    value={resourceContent}
                                    onChange={(event) => handleEditorChange(event)}
                                    theme="snow"
                                    modules={{
                                        toolbar: [
                                          [{ 'header': [1, 2, false] }],
                                          ['bold', 'italic', 'underline','strike', 'blockquote'],
                                          [{ 'color': [] }, { 'background': [] }],
                                          [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                                          ['link'],
                                          ['image'],
                                          ['clean']
                                        ],
                                    }}
                                    formats={[
                                        'header','bold', 'italic', 'underline', 'strike', 'blockquote',
                                        'list', 'bullet', 'indent','link', 'image', 'background', 'color'
                                    ]}
                                    />
                            </Col>
                        </Form.Group>


                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Author*</Form.Label>
                            <Col sm={10}><Form.Control type="text" name='resource_author' className='mw-50' required /></Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Author URL*</Form.Label>
                            <Col sm={10}><Form.Control type="text" name='resource_author_url' placeholder='https://www.example.com/some/link' className='mw-50' required /></Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Resource type*</Form.Label>
                            <Col sm={4}>
                                <Form.Control as="select" name='resource_type' className='mw-50' required>
                                    <option value="post">Post</option>
                                    <option value="ad">Advertisement</option>
                                </Form.Control>
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label column sm={2}>Category*</Form.Label>
                            <Col sm={4}>
                                {emptyCategory && <span className='text-danger fs-12'>This field is required</span>}
                                <Form.Control as="select" className='mw-50' onChange={(event) => handleSelectCategory(event)} required>
                                    {categories.map((item, index) => <option key={item.category_id} value={item.category_id}>{item.category_name}</option>)}
                                </Form.Control>

                            </Col>
                        </Form.Group>

                        <Row>
                            <Col sm={2}></Col>
                            <Col sm={6}>
                                <div className='d-flex mw-50' style={{ flexWrap: 'wrap' }}>
                                    {selectedCategories && selectedCategories.map(item =>
                                        <Badge key={item.category_id} className='category-tag' variant='dark' onClick={() => handleDeselectCategory(item.category_id)}>{item.category_name} <FiX strokeWidth='3' /></Badge>
                                    )}
                                </div>
                            </Col>
                        </Row>

                        <div className='mt-5'>
                            <span className='text-secondary fs-13 strong'>MEDIA FILE</span>
                            {!media &&
                                <Form.Group className='mt-2'>
                                    <Form.Label htmlFor='file-upload' className='custom-file-upload'><FiPlus strokeWidth='3' /> Upload Media</Form.Label>
                                    <Form.File id='file-upload' hidden onChange={(event) => handleFileUpload(event)} />
                                </Form.Group>
                            }
                            <Row>
                                {media &&
                                    <Col sm={12} md={6} className='p-2'>
                                        <div className='media-box'>
                                            <div className='media-box-delete-btn' onClick={() => showDeleteConfirm('media')}><FiXCircle size='20' color='red' /></div>
                                            {media.type === 'video'
                                                ? <video controls className='mw-100'><source src={URL.createObjectURL(media.file)} /></video>
                                                : <><img src={URL.createObjectURL(media.file)} className='mw-100' /></>
                                            }
                                        </div>
                                    </Col>
                                }
                                {media && thumbnailRequired &&
                                    <Col sm={12} md={6} className='pl-3'>
                                        {media && mediaThumbnail &&
                                            <div className='media-box'>
                                                <div className='media-box-delete-btn' onClick={() => showDeleteConfirm('thumbnail')}><FiXCircle size='20' color='red' /></div>
                                                <img src={URL.createObjectURL(mediaThumbnail)} className='mw-100' />
                                            </div>
                                        }
                                        {media && media.type === 'video' && !mediaThumbnail && <>
                                            <span className=''>Please upload a thumbnail for the video</span>
                                            <Form.Group className='mt-2'>
                                                <Form.Label htmlFor='thumbnail-upload' className='custom-file-upload'><FiPlus strokeWidth='3' /> Upload Thumbnail</Form.Label>
                                                <Form.File id='thumbnail-upload' hidden accept="image/*" onChange={(event) => handleThumbnailUpload(event)} />
                                            </Form.Group>
                                        </>
                                        }
                                    </Col>
                                }
                            </Row>
                        </div>

                        <Button variant='success' disabled={saveLoader} size='sm' className='mt-5' type='submit'>
                            {saveLoader
                                ? <><Spinner animation="border" variant='light' size='sm' /> Saving... </>
                                : <>Create</>
                            }
                        </Button>
                    </Form>
                </div>
            </Container>
            <DeleteConfirm show={deleteConfirm} hide={hideDeleteConfirm} confirm={handleMediaDelete} />
        </Aux>
    );
};

const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResourceDetails));