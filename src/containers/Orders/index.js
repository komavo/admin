import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Table, Badge, Button, Modal, Row, Col, Form } from 'react-bootstrap';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';

import Aux from '../../hoc/_Aux';
import BreadCrumb from '../../components/BreadCrumb';
import Loader from '../../components/Loader';
import Switch from "react-switch";
import axiosInstance from "../../utils/axiosAPI.js";
import "./index.scss";
import moment from 'moment';

const Orders = (props) => {
  const [loading, setLoading] = useState(true);
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [orders, setOrders] = useState();
  const [count, setCount] = useState();
  const [limit, setLimit] = useState(10);
  const [offset, setOffset] = useState(0);

  const [showOrderItems, setShowOrderItems] = useState(false);
  const [orderItems, setOrderItems] = useState();
  const [currentOrder, setCurrentOrder] = useState()

  const handleChangeDeliveryStatus = (order_id, is_delivered) => {
    let data = {
      order_id: order_id,
      is_delivered: !is_delivered,
      limit: limit,
      offset: offset
    }
    axiosInstance.post('/admin/orders/update-delivery-status', { ...data }, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
      .then(res => {
        console.log(res.data);
        setOrders(res.data.data.orders);
        setCount(res.data.data.count);
      })
      .catch(err => {
        console.log(err);
      })
  }
  const handleCancelOrder = (order_id, is_cancelled) => {
    let data = {
      order_id: order_id,
      is_cancelled: !is_cancelled,
      limit: limit,
      offset: offset
    }
    console.log(data);
    axiosInstance.post(`/admin/orders/cancel`, {...data}, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
      .then(res => {
        console.log(res.data);
        setOrders(res.data.data.orders);
        setCount(res.data.data.count);
      })
      .catch(err => {
        console.log(err);
      })
  }

  const handleClose = () => {
    setCurrentOrder(null);
    setOrderItems(null);
    setShowOrderItems(false);
  }
  const handleOpen = (item) => {
    console.log(item);
    setCurrentOrder(item);
    setOrderItems(item.order.items);
    setShowOrderItems(true);
  }


  const fetchOrders = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/admin/orders', { headers: { Authorization: 'Bearer ' + props.user.access_token }, params: { limit: limit, offset: offset } })
        .then(res => {
          console.log(res.data.data);
          setOrders(res.data.data.orders);
          setCount(res.data.data.count);
          resolve()
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    document.title = 'Orders | Komavo Admin';
    setBreadCrumbs([
      { location: "Dashboard", path: '/dashboard' },
      { location: "Order", path: '/order' }
    ]);
    (async () => {
      try {
        await fetchOrders();
        setInterval(fetchOrders, 60000)
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const get_status_badge = (status) => {
    switch (status) {
      case 'pending': {
        return (
          <Badge pill bg='warning'>
            Pending
          </Badge>
        );
      }
      case 'preparing': {
        return (
          <Badge pill bg='warning'>
            Preparing
          </Badge>
        );
      }
      case 'completed': {
        return (
          <Badge pill bg='success'>
            Completed
          </Badge>
        );
      }
    }
  }

  const changePage = (page_type) => {
    let _offset = offset;
    let _limit = limit;

    if (page_type === 'next') {
      _offset += limit;
    }
    else {
      _offset -= limit;
    }

    let params = {
      limit: _limit,
      offset: _offset
    }
    // if ()
    axiosInstance.get('/admin/orders', { headers: { Authorization: 'Bearer ' + props.user.access_token }, params: { ...params } })
      .then(res => {
        console.log(res.data.data);
        setOrders(res.data.data.orders);
        setCount(res.data.data.count);
        setOffset(_offset);
      })
      .catch(err => {
        console.log(err);
      })
  }

  const handleOrderSearch = (e) => {
    if (e.target.value.length > 0) {
      console.log(e.target.value);
      axiosInstance.get(`/admin/orders/search/${e.target.value}`, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          console.log(res.data.data);
          setOrders(res.data.data.orders);
          setCount(res.data.data.count);
        })
        .catch(err => {
          console.log(err);
        })
    }
    setLoading(false);
  }

  return (
    <Aux>
      <Container className='my-2'>
        <BreadCrumb list={breadCrumbs} />
        <div className='text-secondary fs-25'>Orders</div>
        <div>
          <Form.Group as={Row} className='form-group mt-4'>
            <Col sm={6}>
              <Form.Control type="text" name='search' placeholder='Search items' className='mw-100' onChange={(e) => { setLoading(true); handleOrderSearch(e); return }}/>
            </Col>
          </Form.Group>
        </div>
        {loading
          ? <Loader />
          :
          <div className='mt-3'>
            <Table hover size='sm' >
              <thead>
                <tr>
                  <th>Order Id</th>
                  <th>Seller</th>
                  <th>Deliver to</th>
                  <th>Total</th>
                  <th>Ordered at</th>
                  <th>Status</th>
                  <th>Delivery</th>
                  <th>Cancelled</th>
                </tr>
              </thead>
              <tbody>
                {!loading && orders && orders.map((item, index) => {
                  return (
                    <tr key={`order-${index}`}>
                      <td className='fs-13' onClick={() => handleOpen(item)}>{item.order.order_id}</td>
                      <td onClick={() => handleOpen(item)}>{item.seller.store_name}</td>
                      <td onClick={() => handleOpen(item)}>
                        {item.consumer.phone}
                        <br />
                        {item.consumer.first_name} {item.consumer.last_name}
                        <br />
                        {item.consumer.location}
                      </td>
                      <td onClick={() => handleOpen(item)}>{item.order.total}</td>
                      <td onClick={() => handleOpen(item)}>{moment(item.order.created_at).format('Do MMM, YY | h:mm')}</td>
                      <td onClick={() => handleOpen(item)}>{get_status_badge(item.order.status)}</td>
                      <td><Switch onChange={() => handleChangeDeliveryStatus(item.order.id, item.order.is_delivered)} checked={item.order.is_delivered} /></td>
                      <td><Switch onChange={() => handleCancelOrder(item.order.id, item.order.is_cancelled)} checked={item.order.is_cancelled} /></td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
            <Row>
              <Col sm={6}>
                {offset != 0 &&
                  <Button variant='dark' onClick={() => { changePage('prev') }}><FiChevronLeft size='20' /> Previous</Button>
                }
              </Col>
              <Col sm={6}>
                {offset + limit < count &&
                  <div className='d-flex justify-content-end align-items-end'>
                    <Button variant='dark' onClick={() => { changePage('next') }}>Next <FiChevronRight size='20' /></Button>
                  </div>
                }
              </Col>
            </Row>
            {/* {count > 0 &&
              <>
                <ReactPaginate
                  pageCount={count / 5}
                  pageRangeDisplayed={3}
                  containerClassName='pagination'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  activeClassName='active'
                  onPageChange={(e) => changePage(e)}
                />
              </>
            } */}
          </div>
        }
      </Container>
      {showOrderItems && currentOrder &&
        <Modal show={showOrderItems} onHide={handleClose} backdrop="static" style={{ marginTop: 100 }}>
          <Modal.Body className='light-text' style={{backgroundColor: "#222"}}>
            <div className='fs-20 mb-4 light-text'>Order Details</div>
            <Row>
              {/* <Col sm={3}>
                <img src={`${process.env.REACT_APP_S3_PREFIX}${currentOrder.seller.store_image}`} className='mw-100'></img>
              </Col> */}
              <Col sm={9}>
                <span className='fs-20' style={{fontWeight: 600}}>{currentOrder.seller.store_name}</span>
              </Col>
            </Row>
            {orderItems &&
              <>
                <Row className='mt-3'>
                  <Col xs={6}>Item Name</Col>
                  <Col xs={2}>Qty</Col>
                  <Col xs={2}>Price</Col>
                  <Col xs={2}></Col>
                </Row>
                {orderItems.map((item, index) => {
                  return (
                    <Row key={`order-item-${index}`}>
                      <Col xs={6}>{item.name}</Col>
                      <Col xs={2}>{item.quantity}</Col>
                      <Col xs={2}>{item.price}</Col>
                      <Col xs={2}>{item.quantity * item.price}</Col>
                    </Row>
                  )
                })}
              </>
            }
            <div className='my-2' style={{display: "block", height: 2, width: '100%', backgroundColor: '#000'}}></div>
            <Row className=''>
              <Col xs={6}></Col>
              <Col xs={2}></Col>
              <Col xs={2}>Delivery</Col>
              <Col xs={2}>{currentOrder.order.delivery_charge}</Col>
            </Row>
            <Row className=''>
              <Col xs={6}></Col>
              <Col xs={2}></Col>
              <Col xs={2}>Total</Col>
              <Col xs={2}>{currentOrder.order.total}</Col>
            </Row>

            <div className='mt-4 d-flex justify-content-center align-items-center'>
              <Button onClick={handleClose} variant='danger'>Okay</Button>
            </div>
          </Modal.Body>
        </Modal>
      }
    </Aux>
  );
};


const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Orders));