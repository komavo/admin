import React, { useState, useEffect } from 'react';
import { withRouter, useHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Row, Col, Table, Badge, Dropdown, Button } from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import BreadCrumb from '../../components/BreadCrumb';
import DeleteConfirm from '../../components/DeleteConfim';
import CommentReplyModal from '../../components/CommentReplyModal';
import axiosInstance from '../../utils/axiosAPI';
import { FiX, FiCheck, FiAlertTriangle, FiChevronLeft, FiChevronRight, FiTrash, FiInfo } from 'react-icons/fi';
import './index.scss';
import moment from "moment";

function UserDetails(props) {
    const history = useHistory();

    const [breadCrumbs, setBreadCrumbs] = useState();
    const [postData, setPostData] = useState();
    const [loading, setLoading] = useState(true);
    const [postStatus, setPostStatus] = useState();

    const [deleteConfirm, setDeleteConfirm] = useState(false);
    const showDeleteConfirm = () => { setDeleteConfirm(true) };
    const hideDeleteConfirm = () => { setDeleteConfirm(false) };
    const [deleteType, setDeleteType] = useState();

    const [replyCommentID, setReplyCommentID] = useState();
    const [commentReplyModal, setCommentReplyModal] = useState(false);
    const showCommentReplyModal = () => { setCommentReplyModal(true) };
    const hideCommentReplyModal = () => { setCommentReplyModal(false) };

    const handleDeletePost = () => {
        axiosInstance.post('/posts/' + postData.post_id + '/reject/', { type: "post" }, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
            .then(res => {
                history.push('/posts');
            })
    }
    const deleteComment = (id) => {
        axiosInstance.post('/posts/' + id + '/reject/', { type: "comment" }, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
            .then(res => {
                window.location.reload();
            })
    }
    
    const deleteCommentReply = (comment_id, reply_id) =>{
        const data = {
            type: "comment-reply",
            comment_id: comment_id
        } 
        axiosInstance.post('/posts/' + reply_id + '/reject/', data, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
            // window.location.reload();
            let temp = postData;
            let comment_index;
            temp.comment.some((item, index) => { 
                if(item.post_comment_id === comment_id){
                    comment_index = index;
                    return false;
                }
            })
            console.log(comment_index);
            temp.comment[comment_index].comment_reply = temp.comment[comment_index].comment_reply.filter(item => item.post_comment_reply_id !== reply_id);
            console.log(temp);
            setPostData({...temp});
        })
    }
    const handlePostPagination = (direction) => {
        if (direction === "previous") {
            history.push(`/posts/details/${postData.next_prev.prev}`);
            window.location.reload();
        }
        else {
            history.push(`/posts/details/${postData.next_prev.next}`);
            window.location.reload();
        }
    }
    const fetchUserDetails = (id) => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/posts/' + id + '/', { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'multipart/form-data' } })
                .then(res => {
                    console.log(res.data);
                    setPostData(res.data);
                    setPostStatus(res.data.approved);
                    setBreadCrumbs([
                        { location: "Dashboard", path: '/dashboard' },
                        { location: "Posts", path: '/posts' },
                        { location: res.data.post_title, path: '/users' }
                    ]);
                    resolve()
                })
                .catch(err => {
                    reject();
                })
        })
    }
    const setPostReviewed = (id) => {
        axiosInstance.get('/set-reviewed/'+ id +'/', { headers: { 'Authorization': 'Bearer ' + props.user.access_token} })
    }
    const setAddedReply = (data) =>{
        let temp = postData;
        let comment_index;
        temp.comment.some((item, index) => { 
            if(item.post_comment_id === data.comment.post_comment_id){
                comment_index = index;
                return true;
            }
        })
        temp.comment[comment_index].comment_reply.push(data);
        setPostData(temp);
        setReplyCommentID(null);
    }
    const setAddedComment = (data) =>{
        let temp = postData;
        temp.comment.push(data);
        setPostData(temp);
    }
    useEffect(() => {
        document.title = 'Post Details | Diall Admin';
        setLoading(true);
        setPostReviewed(props.match.params.id);
        (async () => {
            try {
                await fetchUserDetails(props.match.params.id);
            }
            catch (e) {
                console.log(e);
            }
            finally {
                setLoading(false);
            }
        })();
    }, []);
    return (loading ? <Loader />
        :
        <Aux>
            <Container className='px-0 mt-5 mb-5'>
                <Row>
                    <Col sm={6} >
                        <BreadCrumb list={breadCrumbs} />
                    </Col>
                    <Col sm={6} >
                        <div className='float-right'>
                            {postData.next_prev.prev && <Button variant='light' size="sm" className='post-detail-pagination-btn' onClick={() => handlePostPagination("previous")}><FiChevronLeft strokeWidth='5' /> Previous</Button>}
                            {postData.next_prev.next && <Button variant='light' size="sm" className='post-detail-pagination-btn' onClick={() => handlePostPagination("next")}>Next <FiChevronRight strokeWidth='5' /></Button>}
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col sm='8'>
                        <div className='bg-white p-3 brad-10'>
                            {postData.admin_reviewed && <span className='text-secondary fs-13'><FiInfo strokeWidth='3' /> This post has been reviewed</span>}
                            <Row className='mt-4'>
                                <Col sm='2 strong text-secondary fs-14'>Title</Col>
                                <Col sm='10  text-secondary fs-14'>{postData.post_title}</Col>
                            </Row>
                            <Row className='mt-4'>
                                <Col sm='2 strong text-secondary fs-14'>Content</Col>
                                <Col sm='10  text-secondary fs-14'>{postData.post_content}</Col>
                            </Row>

                            <Row className='mt-4'>
                                <Col sm='2 strong text-secondary fs-14'>City</Col>
                                <Col sm='10  text-secondary fs-14'>{postData.post_city}</Col>
                            </Row>
                            <Row className='mt-4'>
                                <Col sm='2 strong text-secondary fs-14'>State</Col>
                                <Col sm='10  text-secondary fs-14'>{postData.post_state}</Col>
                            </Row>
                            <Button variant="danger" size='sm' className='mt-5' onClick={() => { setDeleteConfirm(true) }}><FiTrash strokeWidth="3" className='mr-2' />Delete this post</Button>
                        </div>
                    </Col>
                    <Col sm='4'>
                        <div className='bg-white p-3 brad-10'>
                            {postData.post_media.length === 0
                                ? <span className='strong text-secondary fs-13'>NO MEDIA</span>
                                :
                                <>  <span className='strong text-secondary fs-13'>MEDIA CONTENT</span>
                                    <div className='mt-2'></div>
                                    {postData.post_media.map((media, index) => {
                                        return <div key={index} className='mt-2'>
                                            {media.post_media_type === 'image'
                                                ? <><img src={media.full_url} className='mw-100' /></>
                                                : <><video width="100%" controls><source src={media.full_url} /></video></>
                                            }
                                        </div>
                                    })
                                    }
                                </>
                            }
                        </div>
                    </Col>
                </Row>

                {/* RELATED COMMENTS */}
                <div className='mt-4 mx-auto' style={{maxWidth: "75vw"}}>
                    <span className='text-secondary strong fs-13'>COMMENTS</span>
                    {postData && postData.comment.length === 0
                        ? <div className='fs-12'>
                            <span className='text-secondary'>No Comments found</span>
                            <Button variant="" className='text-info' onClick={()=>showCommentReplyModal()}>Add comment</Button>
                        </div>
                        : <>
                            {postData.comment.map((comment, index) =>
                                {
                                    return comment.approved === "approved" 
                                    ?   
                                        <div>
                                            <div className='comment-card' key={`comment-${comment.post_comment_id}`}>
                                                <Row>
                                                    <Col sm={10} md={10}>
                                                        <div>{comment.post_comment_content}</div>
                                                        <div> <span className='text-secondary fs-13'>{comment.post_comment_title}</span> <span className='text-muted'>|</span> <span className='text-secondary fs-13'>{moment(comment.created_at).format("MMMM Do, YYYY")}</span></div>
                                                    </Col>
                                                    <Col sm={2} md={2}>
                                                        <div className='d-flex justfi-content-center align-items-center h-100'>
                                                            <Button variant='' className='text-info' onClick={() => { setReplyCommentID(comment.post_comment_id); showCommentReplyModal()}}>Add reply</Button>
                                                            <Button variant='light' onClick={() => deleteComment(comment.post_comment_id)}><FiTrash strokeWidth="3" color="red"/></Button>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                            {comment.comment_reply.map((reply, i)=>{
                                                return reply.approved === "approved" ? (
                                                    <div key={`reply-${reply.post_comment_reply_id}`} className='ml-5 comment-card'>
                                                        <Row>
                                                            <Col sm={11} md={11}>
                                                                {reply.reply_person} {reply.reply_content}
                                                                <div> <span className='text-secondary fs-13'>{reply.reply_title}</span> <span className='text-muted'>|</span> <span className='text-secondary fs-13'>{moment(reply.created_at).format("MMMM Do, YYYY")}</span></div>
                                                            </Col>
                                                            <Col sm={1} md={1}>
                                                                <Button variant='light' onClick={()=>deleteCommentReply(reply.comment.post_comment_id, reply.post_comment_reply_id)}><FiTrash strokeWidth="3" color="red"/></Button>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )
                                                : null
                                            })}
                                            <hr className='d-block my-3'/>
                                        </div>
                                    : null
                                }
                                )
                            }
                            <Button variant="" className='text-info' onClick={()=>showCommentReplyModal()}>Add comment</Button>
                        </>
                    }
                </div>

            </Container>
            <DeleteConfirm show={deleteConfirm} hide={hideDeleteConfirm} confirm={handleDeletePost} />
            <CommentReplyModal
                show={commentReplyModal}
                hide={hideCommentReplyModal}
                commentID={replyCommentID}
                postID={postData.post_id}
                access_token={props.user.access_token}
                setReply={setAddedReply}
                setComment={setAddedComment}
            />
        </Aux>
    );
};

const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserDetails));
