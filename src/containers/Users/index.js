import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { 
  Container, 
  Table,
  Row,
  Col,
  Button,
  Form
} from "react-bootstrap";
import { 
  FiChevronRight, 
  FiChevronLeft 
} from "react-icons/fi";
import Aux from "../../hoc/_Aux";
import BreadCrumb from "../../components/BreadCrumb";
import Loader from "../../components/Loader";
import axiosInstance from "../../utils/axiosAPI";
import moment from "moment";

const Users = (props) => {
  const history = useHistory();

  const [loading, setLoading] = useState(true);
  const [breadCrumbs, setBreadCrumbs] = useState();
  const [users, setUsers] = useState(0);
  const [count, setCount] = useState();
  const [limit, setLimit] = useState(20);
  const [offset, setOffset] = useState(0);


  const changePage = (page_type) => {
    let _offset = offset;
    let _limit = limit;

    if (page_type === 'next') {
      _offset += limit;
    }
    else {
      _offset -= limit;
    }

    let params = {
      limit: _limit,
      offset: _offset
    }
    // if ()
    axiosInstance.get('/admin/users', { headers: { Authorization: 'Bearer ' + props.user.access_token }, params: { ...params } })
      .then(res => {
        console.log(res.data.data);
        setUsers(res.data.data.users);
        setCount(res.data.data.count);
        setOffset(_offset);
      })
      .catch(err => {
        console.log(err);
      })
  }

  const handleUserSearch = (e) => {
    let query = '';
    if (e.target.value.length === 0) {
      query = 'fetch_all'
    }
    else {
      query = e.target.value;
    }
    axiosInstance.get(`/admin/users/search/${query}`, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        console.log(res.data.data);
        setUsers(res.data.data.users);
        setCount(res.data.data.count);
      })
      .catch(err => {
        console.log(err);
      })
    setLoading(false);
  }

  const fetchUsers = () => {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get("/admin/users", {
          headers: { Authorization: "Bearer " + props.user.access_token },
        })
        .then((res) => {
          console.log(res.data.data)
          setUsers(res.data.data.users);
          setCount(res.data.data.count);
          resolve();
        })
        .catch((err) => {
          reject();
        });
    });
  };
  useEffect(() => {
    document.title = "Users | Diall Admin";
    setBreadCrumbs([
      { location: "Dashboard", path: "/dashboard" },
      { location: "Users", path: "/users" },
    ]);
    (async () => {
      try {
        await fetchUsers();
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <Aux>
      <Container className="my-2">
        <BreadCrumb list={breadCrumbs} />
        <div className="text-secondary fs-25">Users</div>
        <div>
          <Form.Group as={Row} className='form-group mt-4'>
            <Col sm={6}>
              <Form.Control type="text" name='search' placeholder='Search items' className='mw-100' onChange={(e) => { setLoading(true); handleUserSearch(e); return }} />
            </Col>
          </Form.Group>
        </div>
        {users && (
          <div className="mt-3">
            <Table hover size="sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Phone</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Address</th>
                  <th>Joined</th>
                </tr>
              </thead>
              <tbody>
                {users &&
                  users.map((item, index) => {
                    return (
                      <tr key={`seller-${index}`}>
                        <td>{item.id}</td>
                        <td>{item.phone}</td>
                        <td>{item.first_name}</td>
                        <td>{item.last_name}</td>
                        <td>{item.location}</td>
                        <td>{moment(item.created_at).format('Do MMM, YY hh:mm A')}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
            <Row>
              <Col sm={6}>
                {offset != 0 &&
                  <Button variant='dark' onClick={() => { changePage('prev') }}><FiChevronLeft size='20' /> Previous</Button>
                }
              </Col>
              <Col sm={6}>
                {offset + limit < count &&
                  <div className='d-flex justify-content-end align-items-end'>
                    <Button variant='dark' onClick={() => { changePage('next') }}>Next <FiChevronRight size='20' /></Button>
                  </div>
                }
              </Col>
            </Row>
          </div>
        )}
      </Container>
    </Aux>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users));
