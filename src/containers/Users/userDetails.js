import React, { useState, useEffect } from 'react';
import { withRouter, useHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Row, Col, Form, Button} from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import BreadCrumb from '../../components/BreadCrumb';
import DeleteConfirm from '../../components/DeleteConfim';
import axiosInstance from '../../utils/axiosAPI';
import { FiArrowLeft } from 'react-icons/fi';
import moment from 'moment';

function UserDetails(props) {
    const history = useHistory();

    const [breadCrumbs, setBreadCrumbs] = useState();
    const [userData, setUserData] = useState();
    const [loading, setLoading] = useState(true);

    const [deleteConfirm, setDeleteConfirm] = useState(false);
    const hideDeleteConfirm = () => {setDeleteConfirm(false);}
    
    const handleDelete = () =>{
      axiosInstance.delete('/users/' + userData.user_id + '/', { headers: {Authorization:'Bearer ' + props.user.access_token}})
      .then(res =>{
          history.push('/users');
      })
    }

    const fetchUserDetails=(id)=>{
        return new Promise((resolve, reject) => {
            axiosInstance.get('/users/' + id + '/', { headers : { 'Authorization' : 'Bearer ' + props.user.access_token, 'content-type' : 'multipart/form-data' }})
            .then(res => {
                setUserData(res.data);
                console.log(res.data);
                setBreadCrumbs([
                    { location : "Dashboard", path: '/dashboard' },
                    { location : "Users", path : '/users' },
                    { location : res.data.email, path : '/users' }
                ]);
                resolve()
            })
            .catch(err => {
                reject();
            })
        })
    }

    useEffect(() => {
        document.title = 'User Details | Diall Admin';
        setLoading(true);
        (async () => {
            try{
                await fetchUserDetails(props.match.params.id);
            }
            catch(e){
                console.log(e);
            }
            finally{
                setLoading(false);
            }
        })();
    }, []);
    return(loading ? <Loader />
    :
        <Aux>
            <Container className='px-0 mt-5'>
            <BreadCrumb list={breadCrumbs} />
            <Form>
                <Row className='mt-2'>
                    <Col xs={12} md={3}>
                        <div className='d-flex align-items-center justify-content-center'>
                            <div className="d-flex justify-content-center align-items-center flex-column" style={{ width:"240px", padding:"20px", backgroundColor:"white", borderRadius:"6px"}}>
                                <img src={userData.profile_pic_url} style={{maxWidth: 150}} alt=""/>
                                {/* <Form.File id="custom-file" label={ userAvatar ? userAvatar.name : "Select profile picture" } custom  className="mt-5" onChange={(event) => handleProfileUpload(event)} /> */}
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} md={9} className='bg-white'>                            
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Name</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.full_name}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Email</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.email}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Mobile</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.mobile}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Date of birth</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.date_of_birth}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Gender</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.gender}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Race</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.race}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Address</Col>
                                        <Col sm="10 text-secondary fs-14">{userData.address}</Col>
                                    </Row>
                                    <div className='light-divider'></div>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Created at</Col>
                                        <Col sm="10 text-secondary fs-14">{moment(userData.created_at).format('MMMM Do, YYYY')}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Updated at</Col>
                                        <Col sm="10 text-secondary fs-14">{moment(userData.updated_at).format('MMMM Do, YYYY')}</Col>
                                    </Row>
                                    <Row className='mt-3 mb-3'>
                                        <Col sm='2 strong text-secondary fs-14'>Last visit</Col>
                                        <Col sm="10 text-secondary fs-14">{moment(userData.last_visit).format('MMMM Do, YYYY')}</Col>
                                    </Row>
                    </Col>
                </Row>
                <div className="pt-5 p-2 d-flex">
                    <Link to='/users' ><Button variant='secondary' size='sm' className='d-flex justify-content-center align-items-center' ><FiArrowLeft className='mr-2'/>Back</Button></Link>
                </div>
            </Form>
        </Container>
        <DeleteConfirm show={deleteConfirm} hide={hideDeleteConfirm} confirm={handleDelete} />
        </Aux>
    );
};
  
const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};
  
const mapDispatchToProps = dispatch => {
    return {}
};
  
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserDetails));