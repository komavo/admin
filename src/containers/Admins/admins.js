import React, { useState,useEffect } from "react";
import Aux from "../../hoc/_Aux";

import BreadCrumb from "../../components/BreadCrumb";

import { Container, Form, Button, Table, Modal } from "react-bootstrap";

const Admins = () => {
  const [show, setShow] = useState(false);
  const [breadCrumbs, setBreadCrumbs] = useState();


  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    document.title = "Users | Diall Admin";
    setBreadCrumbs([
      { location: "Dashboard", path: "/dashboard" },
      { location: "Admins", path: "/admins" },
    ]);
  }, []);

  return (
    <Aux>
      <Container className="px-0 mt-5">
      <BreadCrumb list={breadCrumbs} />

        <div className="mt-2">
          <Button
            className="btn btn-primary mb-2 float-right"
            onClick={handleShow}
          >
            Add Admin
          </Button>
          <div className="table-responsive-md">
            <Table size="sm">
              <thead>
                <tr>
                  <th>Sr No</th>
                  <th>Email</th>
                  <th>Full Name</th>
                </tr>
              </thead>
              <tbody>
                <tr key="admin-list">
                  <td>1</td>
                  <td>
                    <a className="link">Deepak@hiehq.com</a>
                  </td>
                  <td>Deepak Deshkar</td>
                </tr>
                <tr key="admin-list">
                  <td>2</td>
                  <td>
                    <a className="link">admin1@hiehq.com</a>
                  </td>
                  <td>Admin One</td>
                </tr>
                <tr key="admin-list">
                  <td>3</td>
                  <td>
                    <a className="link">admin2@hiehq.com</a>
                  </td>
                  <td>Admin two</td>
                </tr>
              </tbody>
            </Table>
          </div>
        </div>
      </Container>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create New Admin</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Full Name</Form.Label>
              <Form.Control type="text" placeholder="Enter Name" />
              <Form.Text className="text-muted">
                It will be same for your username
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" />
            </Form.Group>
            <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </Aux>
  );
};

export default Admins;
