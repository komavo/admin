import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Aux from "../../hoc/_Aux";
import Loader from "../../components/Loader";
import BreadCrumb from "../../components/BreadCrumb";
import { Container, Form, Button, Table, Modal } from "react-bootstrap";
import { FiPlus } from 'react-icons/fi';
import * as actionTypes from '../../store/actions';
import axiosInstance from '../../utils/axiosAPI';

const Admins = (props) => {
  const history = useHistory();
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState();
  const [breadCrumbs, setBreadCrumbs] = useState();
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [adminList, setAdminList] = useState();

  const handleCreateAdmin = (event) => {
    event.preventDefault();
    let formData = new FormData();
    formData.append('email', event.target.email.value);
    formData.append('full_name', event.target.full_name.value);
    formData.append('password', event.target.password.value);
    axiosInstance.post('/admins/', formData, { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        console.log(res.data);
        setAdminList(res.data.reverse());
      })
    setShow(false);

  }

  const fetchAdmins = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/admins/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          setAdminList(res.data.reverse());
          resolve()
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    setLoading(true);
    document.title = "Admins | Diall Admin";
    setBreadCrumbs([
      { location: "Dashboard", path: "/dashboard" },
      { location: "Admins", path: "/admins" },
    ]);
    (async () => {
      try {
        await fetchAdmins();
      }
      catch (e) {
        console.log(e);
      }
      finally {
        setLoading(false);
      }
    })();
  }, []);
  return loading ? <div className='h-100 d-flex justiy-content-center align-items-center'> <Loader /></div> : (
    <Aux>
      <Container fluid className="mx-0 mt-5">
        <BreadCrumb list={breadCrumbs} />

        <div className="">
          <div
            className="add-btn"
            onClick={handleShow}
          >
            <FiPlus strokeWidth='4' className='mr-2' /> Add Admin
          </div>
          <div className="mt-2 table-responsive-md">
            <Table size="sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Email</th>
                  <th>Full Name</th>
                </tr>
              </thead>
              <tbody>
                {adminList && adminList.map((item, index) => {
                  return <tr key={index}>
                    <td>{item.user_id}</td>
                    <td>
                      <a id={item.user_id} >{item.email}</a>
                    </td>
                    <td>{item.full_name}</td>
                  </tr>
                }
                )}
              </tbody>
            </Table>
          </div>
        </div>
      </Container>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title className='fs-17'>Create New Admin</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleCreateAdmin}>
            <Form.Group>
              <Form.Label>Full Name</Form.Label>
              <Form.Control name='full_name' type="text" placeholder="Enter Name" required />
            </Form.Group>

            <Form.Group>
              <Form.Label>Email address</Form.Label>
              <Form.Control name='email' type="email" placeholder="Enter email" required />
            </Form.Group>

            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control name='password' type="password" placeholder="Password" required />
            </Form.Group>


            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
            <Button className='ml-3 btn-success  ' type='submit'>
              Create
            </Button>

          </Form>
        </Modal.Body>
      </Modal>
    </Aux>
  );
};


const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateNotifications: (payload) => dispatch({ type: actionTypes.UPDATE_POST_NOTIFICATIONS, payload: payload })
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Admins));