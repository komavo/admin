import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Modal, Button, Form, Row, Col, FloatingLabel, Spinner } from 'react-bootstrap';
import { FiPlus, FiTrash2, FiEdit2, FiUploadCloud, FiCheck } from 'react-icons/fi';
import Switch from "react-switch";
import Aux from '../../hoc/_Aux';
import BreadCrumb from '../../components/BreadCrumb';
import Loader from '../../components/Loader';
import axiosInstance from "../../utils/axiosAPI.js";
import imageToBase64 from 'image-to-base64/browser';
import "./index.scss";

const RawProducts = (props) => {
    const history = useHistory();

    const [loading, setLoading] = useState(false);
    const [breadCrumbs, setBreadCrumbs] = useState();

    const [rawProducts, setRawProducts] = useState();
    const [rawProductsOriginal, setRawProductsOriginal] = useState();
    const [btnLoading, setBtnLoading] = useState(false);
    const [uploadedImage, setUploadedImage] = useState();
    const [editItem, setEditItem] = useState();
    const [editItemModal, setEditItemModal] = useState();
    const [invalidName, setInvalidName] = useState();
    const [invalidPrice, setInvalidPrice] = useState();

    const setData = (items) =>{
        setRawProducts(items);
        setRawProductsOriginal(items);
    }

    const addItem = () => {
        let temp = {
            new_item: true,
            name: "",
            price: 0,
        }
        setEditItem(temp);
        setEditItemModal(true);
    }

    const handleOpen = (item) => {
        console.log(item);
        setEditItem(item);
        setEditItemModal(true);
    }

    const handleClose = () => {
        setEditItemModal(false);
        setEditItem(null);
        setUploadedImage(null);
    }

    const handleUploadImage = (event) => {
        event.preventDefault();
        setUploadedImage(event.target.files[0]);
    }

    const handleEditChange = (e, type) => {
        var temp = { ...editItem };
        if (type === 'name') {
            temp.name = e.target.value;
        }
        if (type === 'price') {
            temp.price = e.target.value;
        }
        setEditItem({ ...temp });
    }

    const deleteCategory = (item) => {
        axiosInstance.delete(`/admin/raw-products/${item.id}`, { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
            .then(res => {
                var temp = [...rawProducts];
                var newTemp = temp.filter((i) => i.id != item.id);
                setData([...newTemp])   
            })
            .catch(err => {
                console.log(err);
            })
    }

    const toBase64 = (image) => {
        return new Promise((resolve, reject) => {
            imageToBase64(URL.createObjectURL(image))
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => reject(err));
        })
    }

    const handleSubmit = async () => {
        let data = { ...editItem };
        if (data.name === "") {
            setInvalidName(true);
            return false;
        }
        if (!data.price || data.price < 0){
            setInvalidPrice(true);
            return false;
        }
        setBtnLoading(true);
        if (uploadedImage) {
            data.image_mime = uploadedImage.type;
            data.image_base64 = await toBase64(uploadedImage);
        }
        if (data.section === 'no-section') data.section = null;
        console.log(uploadedImage);
        console.log(data);
        if (data.new_item) {
            axiosInstance.post('/admin/raw-products', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...rawProducts];
                    temp.push(res.data.data)
                    setData([...temp]);
                    setInvalidName(false);
                    setInvalidPrice(false);
                    setUploadedImage(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => {
                    console.log(err)
                });
        }
        else {
            console.log('whyyyyy')
            axiosInstance.patch('/admin/raw-products', { ...data }, { headers: { 'Authorization': 'Bearer ' + props.user.access_token, 'content-type': 'application/json' } })
                .then((res) => {
                    console.log(res.data);
                    var temp = [...rawProducts];
                    var index = temp.findIndex(item => item.id === res.data.data.id);
                    temp[index] = res.data.data;
                    setData([...temp])
                    setInvalidName(false);
                    setInvalidPrice(false);
                    setUploadedImage(null);
                    handleClose();
                    setBtnLoading(false);
                })
                .catch((err) => {
                    console.log(err)
                });
        }
    }

    const handleSearch = (e) => {
        var products = [...rawProductsOriginal];
        var filteredItems = products.filter(item => item.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1);
        setRawProducts([...filteredItems]);
    }

    const fetchRawProducts = () => {
        return new Promise((resolve, reject) => {
            axiosInstance.get('/admin/raw-products', { headers: { Authorization: 'Bearer ' + props.user.access_token }, })
                .then(res => {
                    console.log(res.data.data);
                    setData(res.data.data);
                    resolve()
                })
                .catch(err => {
                    reject();
                })
        })
    }

    useEffect(() => {
        document.title = 'Raw Products | Komavo Admin';
        setBreadCrumbs([
            { location: "Dashboard", path: '/dashboard' },
            { location: "Raw Products", path: '/raw-products' }
        ]);
        (async () => {
            try {
                await fetchRawProducts();
            } catch (e) {
                console.log(e);
            } finally {
                setLoading(false);
            }
        })();
    }, []);

    return loading ? <Loader /> : (
        <Aux>
            <Container className="my-2">
                <BreadCrumb list={breadCrumbs} />
                <div className='d-flex justify-content-center align-items-center'>
                    <div className='text-secondary fs-25 flex-grow-1'>Raw Products</div>
                    <Button variant='primary' onClick={() => addItem()}><FiPlus size="15" /> Add Item</Button>
                </div>
                <Form className='mt-4'>
                    <Col sm={6}>
                        <Form.Control type="text" name='search' placeholder='Search categories' className='mw-100' onChange={(e) => handleSearch(e)} />
                    </Col>
                </Form>
                <div className='mt-3'>
                    {rawProducts &&
                        <Row>{rawProducts.map((item, index) => {
                            return (
                                <Col key={`menu-category=${index}`} sm={3}>
                                    <div className='category-container'>
                                        {item.image
                                            ? <div className='d-flex align-items-center justify-content-end mw-100'><img src={`${process.env.REACT_APP_S3_PREFIX}${item.image}`} className='mw-100' style={{ height: 250, width: '100%' }} /></div>
                                            : <div style={{ height: 250, border: '2px dashed #444', borderRadius: '10px' }}></div>
                                        }
                                        <div className='flex-grow-1 fs-20'>{item.name}</div>
                                        <div className='flex-grow-1 fs-20'>&#8377; {item.price}</div>

                                        <div className='mt-4 d-flex justify-content-end align-items-center mt-2'>
                                            <Button variant='dark' onClick={() => deleteCategory(item)} style={{ color: "#fa4b4b" }}><FiTrash2 color="#fa4b4b" size={15} />&nbsp;&nbsp;Delete</Button>
                                            <div className='px-2'></div>
                                            <Button variant='dark' onClick={() => handleOpen(item)} style={{ color: "#188DF4" }}><FiEdit2 color="#188DF4" size={15} /> &nbsp;&nbsp;Edit </Button>
                                        </div>
                                    </div>
                                </Col>
                            );
                        })}
                        </Row>
                    }
                </div>
            </Container>


            {editItem &&
                <Modal show={editItemModal} onHide={handleClose} backdrop="static" className='edit-item-modal'>
                    <Modal.Body>
                        <div className='fs-20'>{editItem && editItem.new_item ? 'New ' : 'Edit '}Raw Product</div>
                        {/* ITEM IMAGE */}
                        <Row>
                            <Col sm={4}>
                                <div className='mt-5'>
                                    {editItem.image && !uploadedImage
                                        ? <img src={`${process.env.REACT_APP_S3_PREFIX}${editItem.image}`} style={{ height: 200, width: 200 }} />
                                        :
                                        uploadedImage
                                            ? <><img src={URL.createObjectURL(uploadedImage)} style={{ height: 200, width: 200 }} /></>
                                            : <div style={{ height: 200, width: 200, border: '3px dashed #666', borderRadius: 20 }} className='flex-column d-flex align-items-center justify-content-center'>
                                                No Image
                                            </div>
                                    }
                                    <Form.Group className='mt-2'>
                                        <Form.Label htmlFor='upload-image' className='custom-upload-btn'>Upload Image <FiUploadCloud size='20' /></Form.Label>
                                        <Form.Control id='upload-image' type='file' hidden accept="image/*" onChange={(event) => handleUploadImage(event)} />
                                    </Form.Group>
                                </div>
                            </Col>
                            <Col sm={8}>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    <Col sm={8}>
                                        {invalidName && <span className='text-danger'>Required*</span>}
                                        <FloatingLabel label='Name'>
                                            <Form.Control type="text" name='name' defaultValue={editItem && editItem.name} className='mw-100' required onChange={(e) => handleEditChange(e, 'name')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className='form-group mt-4'>
                                    <Col sm={8}>
                                        {invalidPrice && <span className='text-danger'>Required*</span>}
                                        <FloatingLabel label='Price'>
                                            <Form.Control type="text" name='price' defaultValue={editItem && editItem.price} className='mw-100' required onChange={(e) => handleEditChange(e, 'price')} />
                                        </FloatingLabel>
                                    </Col>
                                </Form.Group>

                                <div className='mt-3 d-flex justify-content-start align-items-center' >
                                    <Button variant="secondary" onClick={handleClose}>
                                        Cancel
                                    </Button>
                                    <Button disabled={btnLoading} variant="success" onClick={() => handleSubmit()} style={{ marginLeft: '20px' }}>
                                        {btnLoading
                                            ? <>Saving Changes ... <Spinner animation='border' variant='light' /></>
                                            : <>Save Changes <FiCheck size='15' style={{ marginLeft: 7 }} /></>
                                        }

                                    </Button>
                                </div>
                            </Col>
                        </Row>

                    </Modal.Body>
                </Modal>
            }
        </Aux>
    );
};


const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RawProducts));