import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Table, } from 'react-bootstrap';
import Aux from '../../hoc/_Aux';
import Loader from '../../components/Loader';
import BreadCrumb from '../../components/BreadCrumb';
import DeleteConfirm from '../../components/DeleteConfim';
import axiosInstance from '../../utils/axiosAPI';
import { FiTrash, FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import ReactPaginate from 'react-paginate';
import * as actionTypes from '../../store/actions';

function Feedback(props) {
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [feedbackList, setFeedbackList] = useState(false);
  const [breadCrumbs, setBreadCrumbs] = useState();

  const [deleteID, setDeleteID] = useState(null);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const showDeleteConfirm = (id) => { setDeleteID(id); setDeleteConfirm(true); }
  const hideDeleteConfirm = () => { setDeleteID(null); setDeleteConfirm(false); }

  const handleDelete = () => {
    axiosInstance.delete('/users/' + deleteID + '/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
      .then(res => {
        setFeedbackList(res.data);
        hideDeleteConfirm();
      })
  }
  const handlePageClick = (data) => {
    const pageRequested = data.selected + 1;
    fetchPaginatedData(pageRequested);
  }
  const fetchPaginatedData = (page) => {
    axiosInstance.get('/feedbacks/', { headers: { Authorization: 'Bearer ' + props.user.access_token }, params: { page: page } })
      .then(res => {
        setFeedbackList(res.data);
      })
      .catch(err => {
        console.log(err);
      })
  }
  const fetchFeedbacks = () => {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/feedbacks/', { headers: { Authorization: 'Bearer ' + props.user.access_token } })
        .then(res => {
          setFeedbackList(res.data);
          resolve()
        })
        .catch(err => {
          reject();
        })
    })
  }

  useEffect(() => {
    setLoading(true);
    document.title = 'Feedback | Diall Admin';
    setBreadCrumbs([
      { location: "Dashboard", path: '/dashboard' },
      { location: "Feedback", path: '/feedbacks' }
    ]);
    (async () => {
      try {
        await fetchFeedbacks();
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const handleGetDetails = (event) => {
    history.push(`/users/details/${event.target.id}`);
  }
  return loading ? <div className='h-100 d-flex justiy-content-center align-items-center'> <Loader /></div> : (
    <Aux>
      <Container fluid className='mx-0 mt-5 mb-5'>
        <BreadCrumb list={breadCrumbs} />
        <div className='mt-2'>
          {feedbackList &&
            <>
              <div className='table-responsive-md'>
                <Table size='sm'>
                  <thead>
                    <tr>
                      <th>User</th>
                      <th>Feedback</th>
                    </tr>
                  </thead>
                  <tbody>
                    {feedbackList.results.map((feedback, index) =>
                      <tr key={feedback.feedback_id}>
                        <td>{feedback.user && feedback.user.email} </td>
                        <td>{feedback.feedback_content}</td>
                      </tr>
                    )
                    }
                  </tbody>
                </Table>
              </div>

              {feedbackList.count > 20 &&
                <ReactPaginate
                  previousLabel={<FiChevronLeft size={15} />}
                  nextLabel={<FiChevronRight size={15} />}
                  breakLabel={'...'}
                  breakClassName={'break-me'}
                  pageCount={Math.ceil(feedbackList.count / 20)}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={0}
                  onPageChange={(event) => handlePageClick(event)}
                  containerClassName={'pagination'}
                  subContainerClassName={'pages pagination'}
                  activeClassName={'active'}
                />
              }
            </>
          }
        </div>
      </Container>
      <DeleteConfirm show={deleteConfirm} hide={hideDeleteConfirm} confirm={handleDelete} />
    </Aux>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    user: state.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateNotifications: (payload) => dispatch({ type: actionTypes.UPDATE_POST_NOTIFICATIONS, payload: payload })
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Feedback));