import React, { useState, useEffect } from 'react';
import Aux from '../../hoc/_Aux';
import {FiArrowRight} from "react-icons/fi"
import { Container, Form, Button, Spinner } from 'react-bootstrap';
import { withRouter, useHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './index.css';
import Loader from '../../components/Loader';
import LogoLight from '../../assets/images/logo-light.svg';
import axiosInstance from '../../utils/axiosAPI';
import * as actionTypes from '../../store/actions';
import qs from 'qs';

function ResetPassword(props) {
  let history = useHistory(); 
  const [email, setEmail] = useState();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(true);
  const [verified, setVerified] = useState();
  const [status, setStatus] = useState('default');

  const handlePasswordChange = (e) =>{
    e.preventDefault();
    if(e.target.password1.value !== e.target.password2.value){
      setError('Passwords didnt match')
      return false
    }
    if(e.target.password1.value.length < 8){
      setError('Password needs to be longer than 8 characters')
      return false;
    }
    setLoading(true)
    axiosInstance.post('confirm-reset-password/', {email: email, password: e.target.password1.value})
    .then(res => {
      setStatus(res.data);
      setLoading(false);
    })
    .catch(err => {
      console.log('There was an error sending reset link');
    })
  }

  const verifyToken = (token) => {
    return new Promise((resolve, reject) => {
      axiosInstance.post('verify-password-reset-token/', {token: token})
      .then(res => {
        console.log(res.data);
        if(res.data.verified){
          setVerified(true);
          setEmail(res.data.email);
        }
        else{
          setVerified(false);
        }
        resolve()
      })
      .catch(err => {
        console.log('There was an error sending reset link');
        reject();
      })
    })
  }
  useEffect(() => {
    const query = new URLSearchParams(props.location.search);
    const token = query.get('token');
    if(token){
      (async() => {
        try{
          verifyToken(token);
        }
        catch (e){
          console.log(e)
        }
        finally{
          setPageLoading(false);
        }
      })();
    }
    else{
      history.push('/login');
    }
  }, []);
  return (
      <Aux>
        <Container fluid className='h-100 bg-dark d-flex justify-content-center align-items-center flex-column'>
            <div className='d-flex justify-content-center align-items-center'>
              <img src={LogoLight} className='brand-logo'/>&nbsp;<span className='brand-name'>Diall</span>
            </div>
            { pageLoading ? <><Loader /></> : 
            <>
              { verified && status === 'default' &&
                <div className='login-box bg-light p-3 mt-2 flex-column d-flex justify-content-center align-items-center'>
                    Please enter your new password
                    <Form className='d-flex justify-content-center align-items-center flex-column p-3' onSubmit={(event) => handlePasswordChange(event)} autoComplete='off'>
                      { error && <span className='text-danger fs-11 m-2'>{ error }</span>}
                      <Form.Control type="password" name='password1' placeholder="New password" />
                      <Form.Control  className='mt-3' type="password" name='password2' placeholder="Confirm password" />
                      <Button  className='mt-3' variant='primary' type='submit' disabled={loading}>
                        { loading 
                        ? <>Please wait... <Spinner className='ml-2' animation='border' size='sm' /> </>
                        : <>Change Password <FiArrowRight /> </>
                        }
                      </Button>
                    </Form>
                </div>
              }
              { verified && status === 'success' && 
                <div className='login-box bg-light p-3 mt-2 d-flex flex-column justify-content-center align-items-center' style={{maxWidth: "500px"}}>
                  Congratulations, your password has been changed successfully. You can proceed to login now
                  <br />
                  <Link to='/login' className='mt-3'>Proceed to login</Link>
              </div>
              }
              { !verified && 
                <div className='login-box bg-light p-3 mt-2 d-flex flex-column justify-content-center align-items-center' style={{maxWidth: "500px"}}>
                  The link has expired or is invalid, please try again
                </div>
              }
            </>
            }
        </Container>
      </Aux>
    );
  };
  
  const mapStateToProps = state => {
    return {
      isAuthenticated: state.isAuthenticated,
      user: state.user
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      updateTokens: (payload) => dispatch({ type: actionTypes.UPDATE_TOKENS, payload: payload })
    }
  };
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResetPassword));