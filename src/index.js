import "./i18next";
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store';
import { BrowserRouter } from 'react-router-dom';
import App from './app';
import LoadingScreen from './components/LoadingScreen';

const app = (
    <Provider store={store}>
        <PersistGate loading={<LoadingScreen />} persistor={persistor}>
            <BrowserRouter basename='/'>
                <App />
            </BrowserRouter>
        </PersistGate>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));